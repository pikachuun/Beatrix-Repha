For brainstorming's sake, as well as for recordkeeping's sake

(This probably doesn't contain everything)

=======================================
Legend
=======================================

Relevant AI Pattern Specific Variables
-----------
SysVar(2) = AI Pattern Storage, Time, Processing Flags
    &15         = AI Subpattern (Var(58))
    &240        = AI Pattern    (Var(57))
    &256        = Phase 2
    &4194304    = Bet[i] P1StateNo Recognition Flag
    &536870912  = Pattern Termination Flag (Var(59)&256)
    &1073741824 = Pattern Storage (Zero[C])/Restoration Switch (Else)

SysVar(3) = Var(55) = AI Pattern Information Flag Storage
    &1          = !Body Normal Helper Acquisition (*)
    &2          =  Body Normal Helper Acquisition (*)
    &4          = Marking Success
    &8          = Enemy,Life Automatically Decrements (*)
    &16         = Enemy,HasClsn1
    &32         = Enemy,HasClsn2
    &64         = Helper,Clsn2+HitBy OK
    &128        = Enemy,Clsn2+HitBy OK
    &256        = Helper,Clsn1+HitDef !SCA
    &512        = Helper,Clsn1+HitDef OK
    &1024       = Enemy,Clsn1+HitDef !SCA
    &2048       = Enemy,Clsn1+HitDef OK
    &4096       = Infiltrative Exploration Activity (probably can be deprecated)
    &8192       = Helper,Clsn2+HitOverride OK
    &16384      = Enemy,Clsn2+HitOverride OK
    &32768      = Enemy,CouldBePaused
    &65536      = P1StateNo OK (&3 && &16)
    &131072     = 110+115 BAN (110 is a blacklisted animation)
    &262144     = Enemy,Reversible (&1280 || &2560 && &32768)
    &524288     = P1StateNo Quasi-OK (&16, opponent has a specific projectile active)
    &1048576    = The opponent is EXTREMELY desperate to escape Forced Declaration of Death
    &2097152    = 5100 Bug Avoidance measures should be taken
    &4194304    = Variable Tampering Quasi-OK
    &8388608    = Forced Declaration of Death OK
    &16777216   = Guard State BAN
    &1073741824 = Phase 2 Beginning Flag
    &2147483648 = Forced Declaration of Death Execution Flag
                  (As soon as possible, enter Def 887, switch pattern to Tav[D]. Ignore whether AI is on or not.)

Pattern X (Length, in frames, unless otherwise specified)
-----------

* Information Bullet Point
    • Subpoint
        % Sub-subpoint
    + Some sort of advantage
    - Some sort of disadvantage
= Unlocking Bullet Point
    = The branch to unlock
} Branching Bullet Point
    > Natively unlocked branch
 (?)≥ Natively locked branch, but will be unlocked upon meeting certain requirements
 (?)≤ Natively unlocked branch, but will be locked upon meeting certain conditions
# Killing Bullet Point


* R may be one of several requirements.
    • H = Hittable (Clsn2 + Armor/HitBy)
    • R = Reversible (Clsn1 + HitDef(SCA/ANY+Pauseable))
    • M = Marking (Self-explanatory; synonymous but not the same as I)
    • I = Influenceable (Clsn2 + HitBy | Reversible. Helpers count as well)
    • N = Normal Helper (Self-explanatory)
    • P = P1StateNo OK (Normal Helper + Clsn1)
    • B = Body Normal Helper (Self-explanatory)
    • * = Pattern-Specific/Otherwise Too Complex (See this pattern/requirement)
* If a branch is already visited, do not visit it again
    • Any exceptions are listed as such
    • Phase 2 has different processing. 

* Terminology:
    • Hittable: Clsn2 + Armor/HitBy
        % var(55)&24768
    • Reversible: Clsn1 + HitDef(SCA/ANY+Pauseable)
        % var(55)&262144
    • Tangible: Hittable | Reversible
        % var(55)&286912
    • Influenceable: Clsn2 + HitBy | Reversible
        % var(55)&262336

=======================================
Pattern Flowchart
=======================================

NULL 0 (∞F)
-----------
* Dedicated Measures-related
* Will also be the AI pattern until helpers have been initialized

=============================
As soon as possible:
=============================

* Fill the helper space
* With 56 helpers, the space will appear as follows:
    Bea_H00[Mashׁgִyha]     <- Overseer, Helper HitBy
    Bea_H01[Heyvenyevt]   <- Lifekeeper
    Bea_H02[Gdyelan]{P0}  <- Parallel Child 00 (Redirect + SkyWorld Manager)
    Bea_H03[Qareqa'e]{RD} <- Redirect Holding A
    Bea_H04[Qareqa'e]{RD} <- Redirect Holding B
    Bea_H05[Qareqa'e]{RD} <- Redirect Holding G
    Bea_H06[Qareqa'e]{P0} <- Parallel Parent 00 | Reverse Target -1
    Bea_H07[Gdyelan]{P1}  <- Parallel Child 01
    Bea_H08[Qareqa'e]{P1} <- Parallel Parent 01 | Reverse Target -5
    Bea_H09[Gdyelan]{P2}  <- Parallel Child 02
    Bea_H10[Qareqa'e]{P2} <- Parallel Parent 02 | Reverse Target -4
    Bea_H11[Gdyelan]{P3}  <- Parallel Child 03
    Bea_H12[Qareqa'e]{P3} <- Parallel Parent 03 | Reverse Target -3
    Bea_H13[Gdyelan]{P4}  <- Parallel Child 04
    Bea_H14[Qareqa'e]{P4} <- Parallel Parent 04 | Reverse Target -2
    Bea_H15[Gdyelan]{P5}  <- Parallel Child 05
    Bea_H16[Qareqa'e]{P5} <- Parallel Parent 05 | Reverse Target -1
    Bea_H17[Gdyelan]{P6}  <- Parallel Child 06
    Bea_H18[Qareqa'e]{P6} <- Parallel Parent 06 | Reverse Target -5
    Bea_H19[Gdyelan]{P7}  <- Parallel Child 07
    Bea_H20[Qareqa'e]{P7} <- Parallel Parent 07 | Reverse Target -4
    Bea_H21[Gdyelan]{P8}  <- Parallel Child 08
    Bea_H22[Qareqa'e]{P8} <- Parallel Parent 08 | Reverse Target -3
    Bea_H23[Gdyelan]{P9}  <- Parallel Child 09
    Bea_H24[Qareqa'e]{P9} <- Parallel Parent 09 | Reverse Target -2
    Bea_H25[Gdyelan]{PA}  <- Parallel Child 0A
    Bea_H26[Qareqa'e]{PA} <- Parallel Parent 0A | Reverse Target -1
    Bea_H27[Gdyelan]{PB}  <- Parallel Child 0B
    Bea_H28[Qareqa'e]{PB} <- Parallel Parent 0B | Reverse Target -5
    Bea_H29[Gdyelan]{PC}  <- Parallel Child 0C
    Bea_H30[Qareqa'e]{PC} <- Parallel Parent 0C | Reverse Target -4
    Bea_H31[Gdyelan]{PD}  <- Parallel Child 0D
    Bea_H32[Qareqa'e]{PD} <- Parallel Parent 0D | Reverse Target -3
    Bea_H33[Gdyelan]{PE}  <- Parallel Child 0E
    Bea_H34[Qareqa'e]{PE} <- Parallel Parent 0E | Reverse Target -2
    Bea_H35[Gdyelan]{PF}  <- Parallel Child 0F
    Bea_H36[Qareqa'e]{PF} <- Parallel Parent 0F | Reverse Target -1
    Bea_H37[Qareqa'e]{R5} <- Parallel Target 0F | Reverse Parent -5
    Bea_H38[Qareqa'e]{R4} <- Parallel Target 0E | Reverse Parent -4
    Bea_H39[Qareqa'e]{R3} <- Parallel Target 0D | Reverse Parent -3
    Bea_H40[Qareqa'e]{R2} <- Parallel Target 0C | Reverse Parent -2
    Bea_H41[Qareqa'e]{R1} <- Parallel Target 0B | Reverse Parent -1
    Bea_H42[Qareqa'e]{TA} <- Parallel Target 0A | Reverse Target -5
    Bea_H43[Qareqa'e]{T9} <- Parallel Target 09 | Reverse Target -4
    Bea_H44[Qareqa'e]{T8} <- Parallel Target 08 | Reverse Target -3
    Bea_H45[Qareqa'e]{T7} <- Parallel Target 07 | Reverse Target -2
    Bea_H46[Qareqa'e]{T6} <- Parallel Target 06 | Reverse Target -1
    Bea_H47[Gdyelan]{R5}  <- Reverse Child -5
    Bea_H48[Gdyelan]{R4}  <- Reverse Child -4
    Bea_H49[Gdyelan]{R3}  <- Reverse Child -3
    Bea_H50[Gdyelan]{R2}  <- Reverse Child -2
    Bea_H51[Gdyelan]{R1}  <- Reverse Child -1
    Bea_H52[Qareqa'e]{SW} <- SkyWorld Helper Holding, Exploitative Exploration
    Bea_H53[Ht'eallevit]  <- Helper HitDef/Projectile
    Bea_H54[Qby'evit]     <- Enemy Marking
    Bea_H55[M'ebed]       <- Brain, Helper ReversalDef
* Grab crosstalk targets

=============================
RoundState = 1:
=============================

NULL[1] (2048F)
-----------
* Destroy Crosstalk Helpers as appropriate
    • Immediately destroy (in the given example) P0~P5
    • Destroy P6~PA in 2-frame intervals (PB at the same time as PA)
    • Destroy R5~R1 in 2-frame intervals
    • After a large delay, destroy PC~PF in 2-frame intervals
* Enemy Helper SelfState Enabled, but crosstalk also enabled upon spawn
    • Even if I just create a normal helper, I'm happy at this point
    • Note that if a body helper is captured, it will be normalized and a clone will be created under most conditions
* Scan for Simple Formula-based GameTime variables only
    • Enemy has a continuous model because it's easy
        • Should be compatible with Cubic Int/(O2)Float variables (O2Float assumes GameTime Reset is turned on or a fresh MUGEN instance)
    • Helpers have both models
* Otherwise, do nothing
* Simply wait for the opponent to finish their intro peacefully

=============================
RoundState = 2:
=============================

-----------------------------
Pattern Aleph (Damaging)
-----------------------------

Aleph[0] (500~7770F)
Formal Description: Information Initialization
-----------
* Body HitBy + HitDef only (except for ReversalDef marking if possible), do not deal damage
    • I will try to get marked by the opponent as soon as possible
    + We can very, very easily satisfy various conditions that require marking durring initial scraping (Bloody Chaos)
    - We lose to anything that doesn't like it (Aka)
* Automatic Life Modification Sensing
    • Since, during this protocol, Beatrix will do almost nothing, it's the ideal place to scout for self-poison/etc.
    • Of course, also have measures against Random + GameTime life sensing to prevent this from being flagged falsely
* Vampirism Detection
    • If Beatrix gets hit/reversed and the opponent heals, we shouldn't allow that to happen in the future.
* Body HitDef Scraping detection
    • If Beatrix gets reversed and the opponent takes damage, we should note it for later. It might be useful.    
* Scout for StateNo variables
    • I personally like a linear formula
* Allow Helper SelfState
    • We aren't really trying to capture a helper at the moment, so the opponent should just do whatever it wants
* Scout for Pre-to-esque variable references (TBA)
    • Random Variable Sensing sucks
    • Additionally scout for root-based references in the case of helpers, etc.
* StateType = S Only

# MCS (12p)
# Tyrfing (12p)
# Coolseven (12p)

} Branching Priorities:
 (H)≥ Aleph[1] / Scraping Initialization (Least conspicuous option)
 (M)≥ Dalet[0] / Marking Initialization
 (P)≥ Bet[i] / State Callback (P1StateNo)
    > Vav[0] / Securing a Helper

Aleph[1] (1230F~)
Formal Description: Scraping Initialization
-----------
* Deal damage every 41 frames
    • Radio-related measure (its damage cancelling waits 40F)
* If the opponent's life SEEMS to be decreasing upon a hit, store the damage value, and reiterate on the best one after each block.
    • If the damage is better, increase the frequency of attacking
    • If the damage is worse, decrease the frequency of attacking
    • If the damage is the same, either increase the frequency of attacking (if it hasn't worsened) or keep the frequency the same (otherwise)
* We will also scan for any armor avoidance measures to be taken, and use them as appropriate.
* 2000F/block to optimize before it's skipped
* 2000F for an optimized block

* Block 1: ([90, 80, 70, 60, 50, 40, 30, 20, 10, 00]% of LifeMax) + 1
* Block 2: [100, 90, 80, 70, 60, 50, 40, 30, 20, 10]% of Life
* Block 3: 2147483647, -2147483648, ±1000, ±100, ±10, 2, -1

# Radio (000.000 Hz)
# Trouillet (12p)

} Branching Priorities:
 (M)≥ Dalet[0] / Marking Initialization
 (P)≥ Bet[i] / State Callback (Crosstalk)
 (N)≤ Vav[0] / Securing a helper
    > Overflow Damage

Aleph[8] (500F)
Formal Description: Overflow Damage
-----------
* If P1StateNo is valid, exert HitBy callback every 8 frames to guarantee damage.
* Deal either 2147483647 or -2147483648 damage at all times
    • Unless otherwise listed, use Projectile-based mechanisms (precision is more important)

* During the first 100F, only use this.
* During the next 80F, supplement with TargetLifeAdd (-> 0 for the first 10, LifeMax for the last 10)
* During the next 80F, supplement with Guard State Callback (7/8 chance to pick P1StateNo, if available)
* During the next 80F, supplement with Undefined State Callback (7/8 chance to pick P1StateNo, if available)
    • The first 20F of this block has a low value
    • The next 20F has a moderate value
    • The last 40F has a high value
* During the next 80F, supplement with MoveType = H State Callback (7/8 chance to pick P1StateNo, if available)
    • If a MoveType = H state isn't found besides common, use common
* During the last 80F, supplement with Instant Death State Callback (7/8 chance to pick P1StateNo, if available)
    • If no Instant Death states exist, either call back 5149 + (random&3) (1/8 chance) or extend MoveType = H State Callback

} Branching Priorities:
 (M)≥ Marking Initialization
 (P)≥ State Callback (Crosstalk)
 (N)≤ Securing a helper
 (I)≥ State Callback (!Crosstalk)
    > HUB WORLD


-----------------------------
Pattern Bet (Callback)
-----------------------------

Bet[i]
Formal Description: State Callback Routing
-----------
* When a Bet pattern is accessed, it will call this pattern instead of what it's intended to be.
    • The actual location it ends up at depends on whether a state has been called successfully or not
    • Exceptions: Fall Instant Death and Normal Helper Callback, which are called after Common
    • Only Observed Callback is called independently of this normal branching.
* These states will use the extra 2nd bit for the purpose of Crosstalk Detection.
    • Upon entering a state, check for &(3 - P1StateNo). Set using |(1 + P1StateNo)
* During all protocols:
    • If P1StateNo OK, Disable Scouting HitBy
    • Every 100F, if a normal helper isn't acquired, additionally perform crosstalk for 16F (alternate between methods)
        % 4F DEK StateNo ≠ PrevStateNo
        % 4F DEK StateNo = PrevStateNo
        % 4F DEK PrevStateNo ≠ StateNo
        % 4F DEK PrevStateNo = StateNo
    • Upon P1StateNo becoming valid, if within one of these patterns, IMMEDIATELY return here. If the deviation flag is on, disable it.
        % The only exception is if 8F hasn't elapsed within that pattern yet, for transition reasons.

} Branching Priorities (a valid state must be present for it to work):
    > HitBy Callback
    > HitDef Callback
    > Instant Death Callback
    > Ultra-Instant Death Callback
 (B)≤ Normal Helper Callback
    > Common/Quasi-General Purpose Callback

Bet[0] (420~630F)
Formal Description: Instant Death Callback
-----------
* Callback Protocol:
    • P1StateNo Valid
        % Every 2 frames, call the opponent's Instant Death states from memory.
        % Use a P1-P1-P1-?D-P1-P1-P1-!D callback hierarchy, if possible (?D = HB if a HitBy state is defined, HD otherwise, vice versa for !D)
    • P1StateNo Invalid
        % Exert a constant HitDef + ReversalDef
* Overarching Block structure: 
    • Exploitative Exploration LifeSet (4*2)
    • Body-Based Storage (3)
    • Helper-Based Storage (3*2)
    • Infiltrative Exploration LifeSet (2*2)
* Extends to 630F if P1StateNo is usable and the opponent is hittable
    • During this time, HitBy -> Instant Death Ayu-Ayu Killer (P1-P1-P1-P2 Structure)

# Blocking Heart

} Branching Priorities
    > Continue State Callback

Bet[1] (1950F~)
Formal Description: HitBy Callback
-----------
* Divided into 2 step blocks (Non-Clsn2 Granting and Clsn2 Granting), further subdivided into 6 steps
* Each block uses either Standard Throwing Attempts, P1StateNo-based Ayu-Ayu Killer, or Armor Penetration
* Each step cycles between all available HitBy states for the purpose of callback
* Length is variable, depends on HitBy state quantity, timing, and some other things
* Additionally has some NoKO breakthrough measures (Life = 0 impersonation, TargetLifeSet 1 if 0)

* Step 0: HitBy -> Standard Throwing Attempts (!Time, DEK, PrevStateNo DEK, Common)
    • Substep 0: !Time-Penetrating
    • Substep 1: DEK (StateNo, PrevStateNo)
    • Substep 2: Common Formula
* Step 1: HitBy -> Ayu-Ayu Killer
    • Substep 0: Low-number Common Replication
    • Substep 1: MT = H Common Replication
    • Substep 2: Large State Usage (2^20~2147303546 range, 1M possible values)
    • Substep 3: Guard State
    • Substep 4: Common MT = H
    • Substep 5: Exploration MT = H
    • Substep 6: Exploration Instant Death
    • Substep 7: Observed Instant Death
* Step 2: HitBy -> Armor Penetration Proj
    • Substep 0: Common HitFallDamage (0F offset)
    • Substep 1: Exploration HitFallDamage (0F offset)
    • Substep 2: Common HitFallDamage (1F offset)
    • Substep 3: Exploration HitFallDamage (1F offset)

* Step 3: Clsn2 -> HitBy -> Standard Throwing Attempts
    • Substeps in this block mimic Step 0~2
* Step 4: Clsn2 -> HitBy -> Ayu-Ayu Killer
* Step 5: Clsn2 -> HitBy -> Armor Penetration Proj

# Todesfall (Ayu-Ayu)

} Branching Priorities
 (P)≥ Overflow Damage
 (M)≥ Marking Initialization
    > Continue State Callback

Bet[2] (990F)
Formal Description: HitDef Callback
-----------
* During the first 750F:
    • P1StateNo Valid
        % Using a 50F decrementing block structure (0F, 10F, 18F, 24F, 28F, 32F, 35F, 38F, 41F...), call back HitDef states
        % Use a P1-P1-P1-HD callback hierarchy, if possible
    • P1StateNo Invalid
        % Exert HitDef-based HitDef callback every 3 frames.
* During the last 240F:
    • P1StateNo Valid
        % Every 4 frames, call back a Clsn1 anim at random
        % Using a 16F block stucture, call back HitDef states
        % Similar hierarchy to first 960F, but only use a HitDef with Clsn1
    • P1StateNo Invalid
        % Exert ReversalDef-based Clsn2 callback every 2 frames
* When HitBy callback isn't called, use an Instant Death throwing technique (ReversalDef).
    • 56.25% of the time, use DEK with !Time Defense Penetration
    • 18.75% of the time, use DEK without !Time Defense Penetration
    • 12.50% of the time, use PrevStateNo DEK
    • 12.50% of the time, use Common State (0, 20, 40, 5900) throwing

* Overarching Block structure: 
    • Exploitative Exploration (4*2)
    • Observed Storage (3)
    • Infiltrative Exploration (2*2)
* If the above block would return nothing, use guesswork (200*i, Quasi-General Purpose)

} Branching Priorities
 (M)≥ Marking Initialization
    > Continue State Callback

Bet[3] (450F)
Formal Description: Common+MoveType=H State Callback
-----------
* During the first 250F:
    • P1StateNo Valid
        % Every 2 frames, call back a common state at random.
        % Use a P1-P1-P1-BD callback hierarchy, if possible
    • P1StateNo Invalid
        % Exert Dual Common State callback every 2 frames.
    • When callback isn't executed, deal precision underflow/overflow damage.
        % Additionally execute ForceStand 25% of the time
* During the next 100F:
    • Every frame, call back either 40, 45, 50, 132, 155, 5080, 5081, 5100, 5110, or 5900 using an appropriate mechanism.
    • During the last 50F, additionally use deadly poison, if Marking succeeded
* When HitBy callback isn't called, use an Instant Death throwing technique (ReversalDef).
    • 56.25% of the time, use DEK with !Time Defense Penetration
    • 18.75% of the time, use DEK without !Time Defense Penetration
    • 12.50% of the time, use PrevStateNo DEK
    • 12.50% of the time, use Common State (0, 20, 40, 5900) throwing

* Overarching Block structure: 
    • Exploitative Exploration (4*2)
    • Observed Storage (3)
    • Infiltrative Exploration (2*2)
* If the above block would return nothing, use guesswork (X00~X09, X00~X90, X000~X009, X000~X090, Quasi-General Purpose)

} Branching Priorities
    > Fall Instant Death

Bet[4] (300~600F)
Formal Description: Fall Instant Death
-----------
* Deal either 2147483647 or -2147483648 fall damage whenever specified, then call back a state to deal it.
* During the first 300F:
    • P1StateNo Valid
        % Call back HitBy states every 5 frames, then use HitDef (Trip = 25% chance)
        % During the first 100F, this will use P2StateNo. Otherwise, P1StateNo will be used on the next frame
    • P1StateNo Invalid
        % Exert a constant HitDef w/ P2StateNo + Fall Damage
    • If no HitBy state is found and the opponent isn't hittable, this will be skipped completely
* During the second 300F:
    • P1StateNo Valid
        % Call back HitDef states every 5 frames, then use ReversalDef
        % During the first 100F, this will use P2StateNo. Otherwise, P1StateNo will be used on the next frame
    • P1StateNo Invalid
        % Exert a constant ReversalDef w/ P2StateNo + Fall Damage
    • If no HitDef state is found and the opponent isn't reversible, this will be skipped completely

* Overarching Block structure: 
    • Exploitative Exploration (4*2)
    • Infiltrative Exploration (2*2)
    • Common (1)
* Whether this is 300F or 600F depends on whether both HitDef and an HPT Acquisition state+P1StateNo are present, or only one is.
    • Both = 600F
    • HitDef = 300F (start at 0F)
    • HPT+P1 = 300F (start at 300F)

} Branching Priorities:
    > Helper Spawning Callback
    > Variable Tampering Callback
    > HUB WORLD

Bet[8] (300~600F)
Formal Description: Ultra-Instant Death State Callback
-----------
* During the first 300F:
    • P1StateNo Valid
        % Every 2 frames, call back a HitDef state at random. SCA only.
        % Use a P1-P1-P1-HD callback hierarchy, if possible
        % When HitDef state isn't being called back, perform Ultra-Instant Death Callback ReversalDef (HPT = 2 (75%), 3 (25%))
    • P1StateNo Invalid
        % Exert Ultra-Instant Death Callback ReversalDef at all times
    • If no SCA HitDef state is found and the opponent isn't reversible, this will be skipped completely
* During the second 300F, if and only if P1StateNo valid:
    • Every 2 frames, call back an HPT Acquisition state at random.
    • Use a P1-P1-P1-BD callback hierarchy, if possible
    • If the opponent is about to acquire HitPauseTime, perform P1StateNo Ultra-Instant Death Callback
    • If no HPT Acquisition state is found, this will be skipped completely

* Overarching Block structure: 
    • Exploitative Exploration (3)
    • Body Observed Storage (2)
    • Helper Observed Storage (2*2)
    • Infiltrative Exploration (1)
* If the above block would return nothing, use guesswork (X00~X09, X00~X90, X000~X009, X000~X090, Quasi-General Purpose)

} Branching Priorities
    > Common State Callback

Bet[E] (300F)
Formal Description: Helper Spawning State Callback
-----------
* Using a constant HitDef+ReversalDef, call back Helper Spawning states
    • During the first half, use Normal Helper Spawning states, if available
    • During the second half, use Player Helper Spawning states, if available
* During the last 50F of each half, if and only if P1StateNo valid:
    • Call back a helper spawning state using P1StateNo every 2 frames instead

* Overarching Block structure: 
    • Exploitative Exploration (3*2)
    • Infiltrative Exploration (2*2)
* If the above block would return nothing, use guesswork (X00~X09, X00~X90, X000~X009, X000~X090, Quasi-General Purpose)

} Branching Priorities
 (*)≥ Continue State Callback (* = Deviation Flag ON)
    > Variable Tampering Callback
    > HUB WORLD
 

-----------------------------
Pattern Gimel (Body Variable Tampering)
-----------------------------
Gimel[0] (1401F)
Formal Description: Body Variable Tampering Initialization 0
-----------
* Variable Cycling-based procedures
* Use a 7F-based cycling structure for each variable (Greatest -> Least format)
    • F0: 2147483647 (if float, 2147483648)
    • F1: Var(X) + LifeMax
    • F2: LifeMax
    • F3: 0
    • F4: Var(X) - LifeMax
    • F5: -LifeMax
    • F6+: -2147483648
* 700F after the initial cycling, reverse the cycle (hold at 2147483647/2147483648)
* On the final frame, perform variable restoration

} Branching Priorities:
    > Continue


Gimel[1] (616F)
Formal Description: Body Variable Tampering Initialization 1
-----------
* Similarity procedures
* Variable protocol for all variables (5 frame intervals):
    • For the first 300F: Var(t/5)
    • For the next 200F: FVar(t/5) [Int should use Ceil/Floor]
    • For the next 25F: SysVar(t/5)
    • For the next 25F: SysFVar(t/5) [Int should use Ceil/Floor]
    • For the next 65F:
        % BR00: 2147483647       | ∞
        % BR01: 2147483647       | 2147483648
        % BR02: Var(X) + LifeMax | FVar(X) + LifeMax
        % BR03: LifeMax          | LifeMax
        % BR04: Var(X) - LifeMax | FVar(X) - LifeMax
        % BR05: -LifeMax         | -LifeMax
        % BR06: -2147483648      | -2147483648
        % BR07: -2147483648      | -∞
        % BR08: 0                | 0
        % BR09: 0                | NaN
        % BR10: GameTime (+ 1)   | GameTime (+ 1)
        % BR11: -GameTime (- 1)  | -GameTime (- 1)
        % BR12: Var(X) @ t = 0   | FVar(X) @ t = 0
* On the final frame, perform variable restoration

} Branching Priorities:
    > Continue

Gimel[2] (1021F)
Formal Description: Body Variable Tampering Initialization 2
-----------
* Pinpoint procedures
* For integers, use a 9F-based cycling structure (Greatest -> Least format, except the final 2)
    • F0: 2147483647
    • F1: Var(X) + LifeMax
    • F2: LifeMax
    • F3: 0
    • F4: Var(X) - LifeMax
    • F5: -LifeMax
    • F6: -2147483648
    • F7: GameTime
    • F8: -GameTime
* For floats, use a 12F-based cycling structure (Greatest -> Least format, except the final 2)
    • F0: ∞
    • F1: 2147483648
    • F2: FVar(X) + LifeMax
    • F3: LifeMax
    • F4: 0
    • F5: FVar(X) - LifeMax
    • F6: -LifeMax
    • F7: -2147483648
    • F8: -∞
    • F9: NaN
    • FA: GameTime
    • FB: -GameTime
* On the frame after each variable's cycle, perform variable restoration

} Branching Priorities:
    > Continue

-----------------------------
Pattern Dalet (Marking)
-----------------------------
Dalet[0] (500F)
Formal Description: Marking Initialization
-----------
* Fool around with Poison
    • Increase it over time, starting with a proportion of LifeMax, then using a proportion of Life
    • We'll also briefly touch on Poison-based Instant Death
* Briefly throw in some simple TargetState stuff
    • Some DEK should be sufficient for initial work
* If the opponent has a guard flag, break through it
    • Use either a P2StateNo = 0 (No CS, unless TargetState active) Hit/ReversalDef or something else

= If this state is ever entered, the below are unlocked:
    • Poison Chipaway
    • Lethal Poison
    • Power Tampering
    • Remote Throwing

> Branching Priorities:
    > State Callback (Crosstalk)
    > Securing a helper
    > State Callback (!Crosstalk)
    > Poison-based Instant Death

Dalet[1] (600F)
Formal Description: Poison-based Instant Death
-----------
* More serious Poison usage
    • If the opponent has a non-zero maximum life fraction, use this maximum life fraction
    • Otherwise, use -2147483648 as usual
* I'll also use Power Tampering after the first 100F
    • First 200F of power tampering: 0
    • Next 200F of power tampering: PowerMax
    • Last 100F of power tampering: Quasi-General Purpose
* If a damage/life variable is present, it will additionally be tampered with
    • %100 = 25~50: Damage -> whatever its maximum recorded value is
    • %100 = 50~75: Life -> 0
    • %100 = 75~00: Life -> Value ± LifeMax
* If the opponent has a guard flag, break through it
    • Use either a P2StateNo = 0 (No CS, unless TargetState active) Hit/ReversalDef or something else

> Branching Priorities:
    > State Callback (Crosstalk)
    > Securing a helper
    > State Callback (!Crosstalk)
    > Impersonation

Dalet[4] (600F)
Formal Description: TargetState Instant Death
-----------
* More serious TargetState usage
    • First 200F: Normal DEK
    • Next 200F: PrevStateNo DEK
    • Next 200F: Common State DEK
    • Otherwise, use -2147483648 as usual
* Method of Instant Death will vary depending on timing
    • %100 = 00~50: Normal Instant Death OK
    • %100 = 60~80: Deadly Poison Allowance
    • %100 = 80~85: Full Regeneration Allowance
    • %100 = 75~00: PrevStateNo = StateNo usage
    • %100 = 90~10: Don't execute if CustomState
* Execute TargetPowerAdd -> 0 at all times
* During the second half of each block, if a StateNo variable is present, tamper it
* If the opponent has a guard flag, break through it
    • Use either a P2StateNo = X Hit/ReversalDef or something else

> Branching Priorities:
    > State Callback (Crosstalk)
    > Securing a helper
    > State Callback (!Crosstalk)
    > Impersonation

-----------------------------
Pattern He (Impersonation)
-----------------------------
He[0]
Formal Description: Suicide Impersonation
-----------
* Variable Cycling-based procedures
    • Cycle using 2147483647 -> V + LifeMax -> LifeMax -> V -> 0 -> V - LifeMax -> -LifeMax -> -2147483648
    • Exceptions: Fall Instant Death and Normal Helper Callback, which are called after Common
    • Only Observed Callback is called independently of this normal branching.
* These states will use the extra 2nd bit for the purpose of Crosstalk Detection.
    • Upon entering a state, check for &(3 - P1StateNo). Set using |(1 + P1StateNo)
* During all protocols:
    • If P1StateNo OK, Disable Scouting HitBy
    • Every 100F, if a normal helper isn't acquired, additionally perform crosstalk for 16F (alternate between methods)
        % 4F DEK StateNo ≠ PrevStateNo
        % 4F DEK StateNo = PrevStateNo
        % 4F DEK PrevStateNo ≠ StateNo
        % 4F DEK PrevStateNo = StateNo
    • Upon P1StateNo becoming valid, if within one of these patterns, IMMEDIATELY return here. If the deviation flag is on, disable it.
        % The only exception is if 8F hasn't elapsed within that pattern yet, for transition reasons.

He[1] (500F)
Formal Description: Armor Killer
-----------
* During the first 50F, enable crosstalk 
* Deal damage to all captured helpers via HPT + Forgery
    • During the first 50F, deal 1 damage
    • During the next 50F, 
    • Exceptions: Fall Instant Death and Normal Helper Callback, which are called after Common
    • Only Observed Callback is called independently of this normal branching.
* These states will use the extra 2nd bit for the purpose of Crosstalk Detection.
    • Upon entering a state, check for &(3 - P1StateNo). Set using |(1 + P1StateNo)
* During all protocols:
    • If P1StateNo OK, Disable Scouting HitBy
    • Every 100F, if a normal helper isn't acquired, additionally perform crosstalk for 16F (alternate between methods)
        % 4F DEK StateNo ≠ PrevStateNo
        % 4F DEK StateNo = PrevStateNo
        % 4F DEK PrevStateNo ≠ StateNo
        % 4F DEK PrevStateNo = StateNo
    • Upon P1StateNo becoming valid, if within one of these patterns, IMMEDIATELY return here. If the deviation flag is on, disable it.
        % The only exception is if 8F hasn't elapsed within that pattern yet, for transition reasons.

-----------------------------
Pattern Vav (Helper Interference)
-----------------------------
Vav[2]
Formal Description: Misc. Helper Interference


-----------------------------
Pattern Zayin (Combinatory)
-----------------------------
Zayin[0] (960F+)
Formal Description: HUB WORLD
-----------
* Branching destination for most patterns
* Will perform scraping, PalNo Impersonation, Life Impersonation
    • Somewhat of a scout for a couple of things
* Scraping functionality (Body HitDef only) is divided into 80F blocks.
    • %80 = 00: Life
    • %80 = 20: -Life
    • %80 = 40: Life (Unfrozen Life = 0)
    • %80 = 60: -Life (Unfrozen Life = 0)
    • If an overall decrease in life is detected:
        % Decrease the timer by 20.
        % Double the frequency of hit attempts.
        % Set counter accordingly.
* Every 80F, decrement PalNo by 1. If it is 1, loop back to 12.
* Alternate between Alive = 1 and Alive = 0.

> Branching Priorities:
 (M)≥ Marking Initialization
 (P)≥ State Callback (Crosstalk)
 (N)≤ Securing a helper
 (I)≥ State Callback (!Crosstalk)
    > Helper Variable Tampering



-----------------------------
Pattern Tav (Endgame)
-----------------------------
Tav[D] (1000F)
Formal Description: Forced Declaration of Death Initialization
-----------
* Seal Scouting HitBy and HitDef
* Constantly occupy helpers
* If the opponent has HitPauseTime
    • Constantly execute Helper ReversalDef (P2PauseTime = 0, Def 0, Ground.Type ≠ None)
    • Constantly execute Helper HitDef (P2ShakeTime = 0, Def 0, Ground.Type ≠ None, HitOnce OK)
* Execute (Super)Pause under certain circumstances
    • If the opponent is about to be reversed, execute
    • If the opponent is about to be hit, execute
    • If the opponent doesn't have the guard flag active, execute
    • Regardless of circumstance, every 10 frames, execute for 3 frames
* Life impersonation is broken down as follows:
    • First 200F: Non-zero (probably Ceil(LifeMax/10.) - 1)
    • Next 400F: Frozen Zero
    • Last 400F: Unfrozen Zero
* During the last 200F, perform TargetLifeAdd (LifeMax)

> Branching Priorities:
 (*)> Forced Declaration of Death Holding [Req = The opponent is paused, NoKO is not active, the opponent doesn't have the guard flag enabled]
    > HUB

Tav[E] (approx. 1200F)
Formal Description: Forced Declaration of Death Animation Processing
-----------
* Constantly occupy helpers
* Constantly execute (Super)Pause

> Branching Priorities:
    > Forced Declaration of Death Execution

Tav[F] (???F)
Formal Description: Forced Declaration of Death Execution
-----------
* Constantly occupy helpers
* Constantly execute (Super)Pause

> Branching Priorities:
    > ???







=============================
RoundState = 3:
=============================
NULL 8
* Constant Freeze (Pause only, unless Enemy,SuperMoveTime = 0, then use SuperPause)
    • !WinKO: If marking is present, unfreeze on frame 10-ish, execute Declaration of Death
* Crosstalk TargetState (DEK)
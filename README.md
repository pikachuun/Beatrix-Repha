# Beatrix-Repha

This is the gitlab README for Beatrix. Please check the other README files for actual content.
To download the newest version from source, click [here](https://gitlab.com/Pikachuun/Beatrix-Repha/-/archive/master/Beatrix-Repha-master.zip).
As this is used to store partial progress, among other things, **I do not guarantee that the versions downloaded from here are stable.**

gitlab専用りどみだよ。「重要なこと」ってtxtファイルはビアちゃんの真りどみ。それを読んで下さい。
ソースコードからダウンロードは[ここ](https://gitlab.com/Pikachuun/Beatrix-Repha/-/archive/master/Beatrix-Repha-master.zip)。
このサイトは開発中の進捗保存し、コード転送し、などなどのため。**それで、ここからダウンロードしてた版は安定しないかものを注意してください。（つまり落ちリスクあり ）**

### Method for older versions (旧版のDL方法):

Click:  
![](https://i.imgur.com/NzZc0Zj.png)  
![](https://i.imgur.com/D49aC56.png)  
![](https://i.imgur.com/aBM4Fa8.png)  
![](https://i.imgur.com/lkmVtWx.png)
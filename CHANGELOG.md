# ver. ב (initial publication phase)
## Bet 26
### .40
##### Main Changes
* Lightened various bits of processing slightly
* Self-Shattering Wind now only summons 2 gears during the attack instead of 3
* The debug HUD will no longer move during an EnvShake
* Strictened the conditions for Aleph[0] lingering if the opponent has an auto-decrementating life pattern
* Damage Reversal is now detected at any time, in any scraping pattern
* Damage Reversal now operates on a global flag that checks if recovery exists upon hit and life is currently aligned to a decrementation pattern
* Adjusted the scraping attack execution and automatic fluctuation tracking conditions to properly factor in previous reaction timings
* Made the conditions for life-synchronized variable detection stricter
* Revised the process of automatic life management modulation life checking
* Added fixed interval recovery countermeasures to all scraping patterns
* During fixed interval recovery countermeasures, the hitshaketime parameter on attacks is now forcibly disabled
* During Samekh[1]'s Ayu-Ayu Killer steps, Fall.Recover will now be set to 0
* Added pattern Samekh[F]
* If the opponent seems to have HitDef/HitBy conditions that depend on impersonation, they will now be replicated throughout all Samekh patterns during Phase 2
* Phase 2 Dalet[1] and Dalet[4] will now use alive impersonation to attempt to bypass the opponent's NoKO/throw escape
##### Bug Fixes
* Fixed that runoff attacks would only execute once every 3F, rather than once every frame
* Fixed that the Enemy Body HitDef Recognition Flags were written incorrectly
### .31
##### Main Changes
* The last helper will now also exert Intro if the root is in an Intro state
* Adjusted the intro interference condition to only use Helper ReversalDef if marking hasn't succeeded or P1StateNo cannot be performed
* Removed the 2048 proj generation cap
* Ayin[0] will now attempt to remove Fall Damage, if the opponent has it
* Adjusted the alive impersonation values during Forced Declaration of Death
* Added Dedicated Measures for Donald Period -God-, C-Kaban (Resistance Strengthening Patch, 2p), Golden Waddle Dee (6p), Amane Seresterite (2p, teamside=2), Blood_Stain (Old Version), Baka Gustab M
##### Bug Fixes
* Fixed that Scraping Life Adjustment didn't correct life back to its previous value after execution
* Fixed that Constant NoKO wasn't being detected properly
* Fixed that LifeMax repair and Pause Flag clearance weren't affected by the Null Resistance Strengthening setting (always ON)
* Fixed that Forced Declaration completely broke if the Null Resistance Strengthening setting was disabled
### .30
##### Pattern-Specific Changes
* Added Patterns He[0], Ayin[0]
* Halved the minimum transition interval of Bet[4]'s state branching
* During Bet[4], the opponent will no longer branch to HitDef states if a HitBy state succeeded in granting fall damage, except during phase 2
* Deprecated ReversalDef P2StateNo callback from Bet[4]
* Reorganized the Fall Damage pattern of Bet[4] such that similar Fall Damage values are written at similar times
* During Ground World Reformation (Tav[0]), Beatrix will now perform several forms of impersonation besides stealth
* Forced Declaration of Death will now wait on opponents with anomalous sysvars to return to an idle state before execution 
* Slightly adjusted the Post-Forced Declaration throwing protocol
* Removed the small poison from Tav[0]
##### Misc. Changes
* Adjusted how Observed State Acquisition processes animelemtime
* Adjusted how Observed States are determined when an opponent has a transitionary animtime
* Anim Stasis will no longer consider turning animations in its calculations
* Removed the condition that prevented guard state-generated helpers from being obsfucated
* When the opponent's body is thrown, any life-synchronized variables will now be tampered such that the resulting life value is 0
* Body SysVar GameTime Defense Penetration no longer will calculate with respect to gametime+1 if the opponent already has the desired alive and palno
* Removed Ultra-Instant Death Throwing from the RS=4 interference protocol (as it is completely meaningless)
##### Bug Fixes
* Fixed that immediately after a ReversalDef, Observed HitBy would create a false positive
* Fixed that Observed HitBy would ignore granted invuln if a different type of invuln was removed
* Fixed that Scraping Life Adjustment wasn't behaving properly
* Fixed that Life-Synchronized Variables could no longer be obtained
* Fixed that RS=3 throwing would never actually occur
### .20
##### Throw-Related Changes
* Body DEK will now refer to the opponent's state variables on an as-needed basis if referring to the opponent's state directly
* Dalet[1] and Dalet[4] will now attempt to strip fall damage, if it exists
* Dalet[1] will now attempt to strip hitpausetime, if it exists
* Multiple ReversalDef HPT Acquisition attempts will no longer execute at the same time
* Helpers with a Guard State Entry point will now return to their normal state after standard normalization procedures
* Unfrozen UID/Normalization will now automatically suppress marking and induce a GT=None throw rather than having to be executed manually
* Unfrozen UID/Normalization will no longer be defaulted to if the Time-Granting Unfrozen Overflow Setting is enabled
* Patterns Tsade[4] and Vav[4] will now be branched to regardless of the Time-Granting Unfrozen Overflow Setting
* If Beatrix hasn't won during RS=4, she will now attempt to throw the opponent to try for Instant Death
* Added a Quasi-General Purpose Body SysVar Tampering Measure for Fate_Repeater_Yukito
##### Throw-Related Bug Fixes
* Fixed that common states wouldn't be properly returned to during throwing
* Fixed that, if MoveContact hitpausetime acquisition succeeded during a throw, StateType and MoveType will not be properly initialized in subsequent attempts
* Fixed that duplicate helper deletion would refer to the incorrect location for determining RS=2 persistence
* Fixed that the General-Purpose Marking Repair Seal Flag had the opposite of the intended behavior
* Fixed that, if any suspicion of the opponent having a special gametime coefficient existed, it would never be reset upon seeing a change in the corresponding potential coefficient
* Fixed that P1StateNo-type sysvar(1) Penetration would never actually execute
##### Misc. Changes
* Slightly adjusted the Crosstalk Target Deletion protocol
* Changed the state numbers of all basic attacks to have more conventional values
* During Exploration, the dummy's HitFall will now be set to 0 at the start of each StateDef
* Damage Calculation will now automatically stop if there seems to be no hope of reaching the desired damage value
* Added a large damage+fall damage pattern to Bet[4]
* If the opponent cannot be influenced during Samekh[8], a marking TargetState will now be executed to reset them
* During Body Variable Tampering, if the opponent hasn't generated an obsfucation helper successfully, Crosstalk will now recapture any Body Normal Helpers
* Added a variable tampering pattern to Gimel[1]
* Helper Occupancy now uses ownpal=0
* Added Dedicated Measures for Grim_Reaper-Mai, Blue_Wing, gozira and goras (Certain Settings)
* Removed an unintended method for satisfying Y-Mode's final condition
##### Misc. Bug Fixes
* Fixed that the Clsn auto-adjustment function referenced the wrong Clsn1 Hitbox
* Fixed that Body HitBy states weren't observed properly
* Fixed that during Root,StateNo Pandora Killer attempts, the value written could be a negative number
* Fixed that Bet[4]'s Common Armor Penetration step was skipped entirely
* Fixed that Bet[2]'s C1Anim screening would use C2Time, not C1Time, as a reference for execution length
* Fixed that, during the random body variable tampering period, no compound interference would be performed
### .10
##### Main Changes
* Auto-decrementation detection will no longer process information if the root is being reversed
* Made the scraping pattern estimation condition stricter for opponents with random life modulation
* Added a parity-based variable tampering pattern to Pe[1]
* Adjusted sysvar(1) penetration processing to automatically include HitDef and ReversalDef as needed
* Adjusted the pattern transition conditions for Zayin[0]
* Added pattern Vav[5]
* Added dedicated measures for Solaris, \(h4to89 [HARD, 7p], G-Orochi [okihaito's patch, 12p]
##### Bug Fixes
* Fixed that body HitDef-related parameters weren't properly recorded during scraping
* Fixed that, if a special coefficient was present but wasn't switched to during gametime formula acquisition, the formula would be dropped
* Fixed that killing an opponent that revives itself during dedicated measures somehow can cause a TU victory
### .02
##### Main Changes
* Body HitBy will now be sealed upon something happening to the body during Initial Scraping
* Added dedicated measures for T-Zetsu, Claroots [1~10p], COMBO-Angel [Old version, 11/12p], STG-G3 [12p] S_Yorihime [12p]
##### Bug Fixes
* Fixed that the brain would pass MT=A to the root, even if the root isn't interested in having a target in the first place
* Fixed that the gametime formula root finding algorithm would crash for quadratic float gametime variables
* Fixed that Infiltrative Exploration would never actually start
* Fixed that, for some reason, Crosstalk didn't execute during Pe[2] for most variable tampering patterns
### .01
##### Main Changes
* Anomalocaris Killer will now set a helper's movetype to MT=H if the opponent's hitflag contains a +
* If, for some reason, the body has acquired a target even though marking could not, the body will now execute target interference during forced declaration
* If the opponent's position is out of bounds, marking will now attempt to correct it if it's not doing anything else
##### Bug Fixes
* Fixed that RoundNotOver would inadvertedly proc during the outtro when the Y-Mode setting is disabled
* Fixed that Forced Declaration wouldn't execute if there was nothing else to do
### .00
##### Main Changes
* Added the Y-Mode setting
* TimerFreeze will no longer be executed if the match's time limit is being approached
##### Bug Fixes
* Fixed that the queueing function introduced in previous bugfixes caused crashes upon character selection
* Fixed that, if the opponent modifies the engine in a way that disables write permissions, Beatrix would get into a never-ending crash loop
## Bet 25
### .10
##### Main Changes
* Greatly improved compatibility versus certain code-using opponents
* The main trigger addition/bootstrap code has been ported from StateDef Buffer Overflow to Command Overflow
* Allowed crosstalk to desynchronize regardless of the opponent's movetype
* Certain gametime formulae will now also report potential gametimes at which the result would be 0
* When approaching a gametime value that could result in sysvar=0-type gametime penetration, crosstalk will now continuously targetstate until the value is reached
* Added a dedicated measure for Sawaki-chan (12p, only if the time limit is less than 20 minutes)
##### Bug Fixes
* Fixed that helper variable tampering's desynchronization flag caused parallel crosstalk's variable tampering to behave strangely
* Fixed that helper variable revolution wouldn't actually proc
### .01
##### Main Changes
* Combined all Fall Instant Death-related parameters into a single flag
* Added a dedicated measure for !Damnation [Unreasonable Mode ON]
##### Bug Fixes
* Fixed that y Velocity references were still somehow broken
* Fixed that ForceStand would proc the C attribute of Body HitDefAttr impersonation
* Fixed that some Crosstalk Interference would also proc Body Marking
* Fixed that marking as a whole somehow broke
* Fixed Forbidden Knowledge's Dedicated Measure
* Fixed that, during dedicated measures with delayed activation that require crosstalk to be deleted, the redirect and admin HOLPs weren't deleted
### .00
##### Forced Declaration of Death
* **Greatly Revised the execution conditions of Forced Declaration of Death**
	* If the opponent hasn't accessed a guard state naturally OR can be paused and marking exists
	* If Frozen Forced Declaration seems to be a viable option
	* If the opponent can be interfered with and have its life set to 0 by certain Instant Death methods
	* If the opponent seems like it can turn or be otherwise interfered with during RS=3
	* If there's nothing else we can do
* Exploration can now detect Guard ChangeStates
* If the opponent cannot be paused, Forced Declaration of Death will now attempt to grant the opponent HitPauseTime
* Forced Declaration will now search for holes in Guard Flag fixation and time suicide appropriately
* Forced Declaration can now also execute using Ayu-Ayu Killer, Instant Death Callback, and Fall Instant Death
* If the opponent looks like they will turn, target acquisition will now be acquired during RS=3 through Position Impersonation
* Slightly adjusted how Unhittable is processed during Forced Declaration
* Added a Quasi-General Purpose Measure for UltimateNoKilledOath [3p]
* Deleted the Dedicated Measure for D0_Magaki [11p]
##### Other Changes
* If a root,state-synchronized helper exists, its state will be altered via reverse CT during state callback patterns
* If a root,state-synchronized helper has a state management sysvar, that helper will now be captured during state callback patterns
* Added dedicated measures for F_Amenta [1~11p], 10007
##### Bug Fixes
* Fixed that the body would exert Pause-related flags with since-obsoleted timing
* Fixed that Anomalocaris Killer would indefinitely pause under certain circumstances
* Fixed that under certain, rare but consistent pretenses, Inverting and Pre-To GameTime Formula Acquisition would crash
* Fixed that FetchClsn would loop indefinitely if a Clsn2 elem somehow isn't found during certain Unfrozen Interference
## Bet 24
### .21
##### Main Changes
* Slightly cleaned some sprites
* If the enemy body constantly exerts TimerFreeze after death, but can be paused, the enemy will now be paused every other frame
* Added unfrozen life=0 impersonation to Phase 2 Samekh[0]
* During Explod Occupancy, Beatrix will now delete a few Occupancy Explods if they would interfere with visual processing
* Added a Quasi-General Purpose State Callback Measure for NORI Type-Thunder
* Added dedicated measures for Pure_Youmu, KokuShiki (7~9p), AhojinGod (5~12p), Goma-ABURA (10p)
##### Bug Fixes
* Fixed that the GameTime Reset setting didn't do anything
* Fixed that Mid-Throw Helper Anim Impersonation didn't work properly
* Fixed that Explod Occupancy's flag didn't persist between frames
* Fixed that SouShiki_X's dedicated measure would stop working during 12p upon reaching the 5th phase (as well as a scaling error that occurred upon KO)
* Fixed that kfm0x05's dedicated measure would be extremely timing-specific for no reason
* Fixed Joker_Shiki (12p)'s, EXELICA (Shooting Star Patch)'s, singeki or tettai's dedicated measures
### .20
##### Main Changes
* Added 1 new skill (Self-Shattering Wind)
* Revised how main helper IDs and addresses are stored and processed
* Slightly adjusted pre-match helper palno tampering
* Added MoveContact Impersonation to a part of Gimel[1]
* Added a quasi-general purpose crash avoidance measure for TofuDonaldJ
* Added dedicated measures for Zenith_White-Darkness (12p). {} (12p)
##### Bug Fixes
* Fixed that -3 Exploration's Post-Tampering tamper repair would cause an infinite loop
* Fixed that MT=A Reverse Crosstalk would only work properly for the last Reverse Crosstalk helper
* Fixed that, during helper const acquisition, an incorrect function would be called
* Fixed that, when the enemy is killed while being thrown, supermovetime wasn't properly deleted
### .14
##### Main Changes
* Revised the config format to allow for extremely large time limits
* The Time Limit setting's maximum has been increased from 30 minutes to 1 hour
* Added dedicated measures for devious distortion deity, Pandora's Box -Eternal-, zombie Mizuchi
* Added a dedicated config file for a certain tournament
##### Bug Fixes
* Fixed that True God Hanyuu's SelfAnimExist condition would fail on the 1st frame
### .13
##### Main Changes
* Added a StopSnd to the start of the Intro
* During observation processing, HitDefAttr is now treated as NUL if no primary attr exists and the observation target's movetype is I
* Anomalocaris Killer will no longer execute for MT=I players
* Normal helpers will now maintain their state while being thrown instead of remaining in the crosstalk state
* Obsfucated helpers will now be regenerated during Bet[0]
* Added 6 dedicated measures
##### Bug Fixes
* Fixed that Observed Helper Information wasn't stored properly
* Fixed that Obsfucated helpers would regenerate with stateno=0
### .12
##### Main Changes
* Added a flag that prevents Crosstalk from executing on body helpers
* Phase 2 Samekh[E] will now try to generate helpers from other helpers after a set period of time
* Added dedicated measures for sin-genjitsu, ONI-MIKO-X [1~4p]
##### Bug Fixes
* Fixed that gametime variable scouting would crash if an accidental double-tap occurred somewhere
* Fixed that Marking/Crosstalk-derived PrevStateNo Instant Death Callback would crash
* Fixed that Body Marking wouldn't actually trigger if MT=A marking wasn't marked for usage
### .11
##### Main Changes
* Added a dedicated measure for DC-RND-KFM
##### Bug Fixes
* Fixed that Exploration's Projectile Nullification didn't behave properly
* Fixed that parsing abs during Exploration would cause the entire engine to shatter into a trillion pieces
* Fixed 4 dedicated measures
### .10
##### Main Changes
* Added patterns Vav[9], Tsade[0]
* Revised the Exploration Storage Framework
* Made the process of calling random Exploration states more efficient
* Exploration will now store states even if they only contain a ChangeState if the destination is 5050/5100/5110
* Fall Instant Death will now auto-adjust itself if the opponent is branching between common states
* Fall Instant Death will now also use 5050/5100/5110/HFD ChangeState callback
* If the camera is too high or too low for an extended period of time, it will now slowly be pulled back to the stage
* Added a dedicated measure for Valentine-Hisui (5p)
* Disabled Variable Screening during dedicated measures
##### Bug Fixes
* Fixed that Dalet[1]'s termination condition was both inverted AND broken
* Fixed that the Initial Helper PalNo tampering slots were misaligned
* Fixed that, when an admin helper is generated from a player that has a facing of 1, p1stateno callback would never execute
* Fixed that Exploration wouldn't restore player information if the state didn't have any useful properties
* Fixed that Exploration wrote to the incorrect offsets for certain interference types
* Fixed that Exploration's DEK nullification caused a crash
* Fixed that 0F animations weren't properly detected
* Fixed that the Ctrl Removal State was somehow tied to a timer variable
* Fixed that Dedicated Measures that required state impersonation wouldn't function properly unless Time impersonation was also performed
* Fixed various dedicated measures
* Fixed that the opponent would sometimes crash when thrown if naive GameTime Penetration caused a throw escape previously
### .00
##### Variable Tampering
* **Greatly Revised Variable Tampering Patterns' flowcharts and structure**
* **Crosstalk Helpers of any type can now perform Post-Helper Processing Variable Tampering**
* Added a setting for Post-Helper Processing Crosstalk Variable Tampering Methodology
* Re-introduced pattern Gimel[2], added patterns Pe[2] and Vav[8]
* Obsfucated body helpers will now have their movetype adjusted by the brain to facilitate variable tampering
* Deprecated the 3-body helper capture system
* Added a switch that prevents multiple body variable tampering steps from being executed when the opponent hasn't moved
* If a HitBy/HitDef state has never been observed before, we will now attempt to force HitBy/HitDef execution through variable tampering
##### Marking
* **All Variable Tampering+Marking Combinations have been moved into phase 2 Dalet patterns**
* Most Marking Interference will now try to execute right before the enemy body does
* Changed the MT=A Marking Permission flag to a Double-Tap permission flag
* Body Target Acquisition will now only activate if we've seen a guard flag and MT=A in tandem
* If the body acquires a target, it will now always use MT=A impersonation
* Dalet[1] will now attempt to kill through small poisoning after deadly poisoning completes
* During Dalet[4], we will now stop executing TargetState every so often if the opponent is already being thrown
* During part of Dalet[4], Redirect Impersonation and Helper SysVar Fixation will now be performed
##### Misc. Revisions
* Refined the process of Crosstalk State Storage
* Parallel and Reverse Crosstalk Execution are no longer specifically bound to the type of crosstalk helper
* Revised the branching conditions for various crosstalk processes
* Brutality will no longer reiterate alongside other helpers
* ID=0 Helpers can now be destroyed by duplicate deletion
* If the opponent has a non-standard HitFlag during Anomalocaris Killer, the Brain's StateType will now change to try and penetrate it
* Added a quasi-general purpose state callback measure for る〜こと
* Added dedicated measures for UN-DC-KFM (12p), Dream_Animals (11p), Specimens Kung Fu Man (11p)
* Dedicated Measure Detection is now no longer limited to the 1st frame
##### Bug Fixes
* Fixed that armor flags are somehow written to the crosstalk cache
* Fixed that, upon executing GameTime Defense Penetration, variables would be marked as uninitialized
* Fixed that -3 Exploration would return errant, infiltrative exploration-related values when Def -3 didn't actually exist
* Fixed that anything related to the enemy would always return to StateDef 0 upon SelfStating after a throw
* Fixed that certain HitDef processing offsets were incorrect
* Fixed that Helper ReversalDefs without an explicitly specified HPT will constantly increase the opponent's HPT
* Fixed that Redirect Helper PlayerIDs weren't saved properly
* Fixed that, rarely, Bet[2] would never finish
* Fixed that Pe[8] would never finish
* Fixed that Zayin[0] would loop indefinitely
* Fixed that Body HitDef Scraping during Dedicated Measures that don't summon helpers always dealt 1 damage
* Fixed that Forced Declaration's branch condition is broken
* Fixed that Forced Declaration will never actually execute pausing
* Fixed that Forced Declaration's sound would cut off inexplicably
## Bet 23
### .1
##### Main Changes
* Added an auto-adjust specifier for both Body and Helper-specific processing
* Implemented detection protocols for Helper NoKO, Helper Proj-type Body NoKO
* Added Root,StateNo-type Variable Tampering Pandora Killer to Bet and Samekh patterns
* Added Redirect Impersonation to Phase 2 Bet[0]
* Added Pattern Gimel[F]
* Pattern execution order has been made a bit more consistent
* Allowed for the ability to specify different AI sets depending on pattern
* Refined the conditions for Watch Mode processing
* Shifted HitDef/Projectile processing from Brutality to the Brain
* Helpers will now no longer have Clsn2 while acquiring HPT through HitDef-based methods
* Exploration and Observed processing will no longer register states with an Unhittable=1 SuperPause as HitBy states
* Redirect Helpers will now inherit Normalcy Penetration protocols from their respective targets
* If Clsn-granting and HitBy/HitDef states somehow match in callback processing, it will no longer repeat these states
* During certain Bet patterns, SuperPause will be executed if the Unhittable flag is detected
* Bet[F]'s Ayu-Ayu Killer projectiles will now wait until C2+HitBy are both accessed
* Dedicated Measures will now force AI ON
* Added dedicated measures for D0_Magaki (11p), AtemiMashiro
* Added a switch for RNG Manipulation during Dedicated Measures
* Certain dedicated measures will now have altered visuals
##### Bug Fixes
* Fixed that Unfrozen Overflow would trigger before the round starts
* Fixed that, if a projectile existed in the 3rd slot, but not the 1st or 2nd slots, the return value of .NextProj could be incorrect
* Fixed that hit velocity-based processing would refer to an incorrect location
* Fixed that Body GameTime Formula Penetration would write formulae corresponding to an incorrect GameTime offset except when the opponent is being reversed
* Fixed that -3 Exploration, under specific pretenses, would crash
* Fixed that Root,StateNo variables wouldn't be detected if the value is erratic
* Fixed that, if the opponent's life was fixed, but they also allowed normal MT=H, a false positive would occur during scraping
* Fixed that life minima stasis wouldn't be processed properly during scraping
* Fixed that Samekh[6]'s branching condition didn't function properly
* Fixed that the Successful Pause Acquisition method flags didn't set properly under certain situations
* Fixed that Helper ReversalDefs wouldn't properly function if the opponent neither is attacking nor is marked properly
### .0
##### GameTime-Related
* **GameTime Formula Acquisition is now code-based**
* Implemented Sign Inversion Formula Sensing for linear equations and quadratic equations (Quad Term,Full Equation)
* Implemented Pre-To Formula (Self/RootVar only for now) Sensing for linear equations (Linear,Constant Terms)
* Implemented GameTime Modulus (Up to 256) Sensing for linear equations (Linear,ConstantAdd,ConstantSub Terms)
* Implemented StateNo/PrevStateNo Coefficient Formula Sensing for quadratic and linear equations
* Implemented GameTime Formula Typo Correction Processing
* Added a Helper=Root variable scrutiny flag to variable processing
* Added a function to interface with formula acquisition
* GameTime Formula Penetration and -3 Exploration will now execute simultaneously if the -3 Exploration setting is enabled
* If a method for throwing/helper capture succeeds, it will consistently be used in future throwing attempts
* Implemented Helper Variable Synchronization-type GameTime Penetration, -3 Infiltration Processing
##### Enemy-Related
* **Greatly revised enemy processing as a whole**
* Added the ability to properly synchronize PrevStateNo in enemy processing
* Implemented settingless Unfrozen Overflow
* If a method for acquiring HitPauseTime succeeds during throwing, it will consistently be used in future throwing attempts
##### Reorganization-Related
* Reorganized various states' placement in the file structure
* Split beatrix.cns into beatrix.cns (Constants), beatrix.cmd (Commands+Static DEK), and beatrix_aes.st (main aesthetic processing)
* Deleted beatrix_nul.st, beatrix_gmt.st, and beatrix_var.st
* Reorganized Marking and Crosstalk flag placement
* Marking is now exclusively handled by the MT=I marking helper
* Added some processing to set the marking helper's movetype to A and reiterate if it's possible to obtain a target twice
* Allowed Reverse Crosstalk to use MT=A processing under certain conditions
##### Misc. Revisions
* Added Patterns Ayin\[4\] (Unfrozen Ultra-Instant Death) and Ayin\[5\] (Persistent-Granting Ultra-Instant Death Callback)
* Added detection processes for variables that monitor Beatrix's Life and StateNo
* Beatrix's Life Impersonation pattern will now change if a variable monitoring her life is present
* Added a special processing branch for any scraping-related QGPs that rely on extended variables
* Added Ctrl Removal Processing to Bet[X] callback patterns
* 5100 bug avoidance now uses gethitvar(recovertime) instead of const(data.liedown.time)
* Phase 2 Bet[F]'s P2StateNo Callback pattern will now use P2StateNo impersonation
* Dalet[4] will now attempt to acquire a target through Beatrix's Body
* Added ALSIEL-type ChangeState triggering measures to Dalet[4]
* If an opponent has an active ReversalDef, as well as an already existing target, Beatrix will no longer use SCA HitDef with a player that isn't that target
* QNaN tampering during Vav[1] is now formula variable-specific
* Added Root and Helper Variable Synchronization-Type Formula Penetration to Vav[1]
* Added Float Non-Formula Tampering (fvalue=Infinity) to Vav[1]
* Assigned SysVar(1) GameTime Formula Penetration to a flag, rather than a specific pattern
* If the opponent can interrupt our anti-reiteration processing, it will now be stubbed to right before it would be interrupted
* Added a Quasi-General Purpose Exploration Blacklist measure for Lunatic_Empress(\_X)
* Added a Quasi-General Purpose Invulnerability Granting measure for Ideon
* Added Dedicated Measures for !marisa, Dentor-kun
* Deleted the Dedicated Measure for Scene (Nameless Poporo's Patch, 12p)
##### Bug Fixes
* Fixed that EnhancedDEK wouldn't properly parse the enemy's StateDefs if a second opponent didn't exist
* Fixed that Exploration would reset 4 times as many persistent bytes as it needed to
* Fixed that Infiltrative Exploration would use the enemy's anims, falsely flagging SelfAnimExist
* Fixed that Infiltrative Exploration's Loop Counter wasn't reset between iterations
* Fixed a strange bug that caused Exploration to crash with a Type Mismatch: 0 error
* Fixed that, if the opponent had HitFall and a certain cancellation bit was proc'd, they'd become effectively invulnerable
* Fixed that Crosstalk Ultra-Instant Death Callback ignored the enemy's Clsn1 when determining allowance
* Fixed that, if the opponent somehow spawned a projectile in a slot after state callback, the risk of crashing during Crosstalk Ultra-Instant Death Callback went up
* Fixed that during Samekh[3], a freeze would occur when trying to callback Helper States
* Fixed that Pe[8]'s Armor Killer step didn't properly impersonate damage
* Fixed that Redirect Impersonation would refer to its target's helperID as its playerID
* Fixed that Ctrl Impersonation wouldn't behave properly during dedicated measures if Beatrix didn't already have Ctrl
## Bet 22
### .0
* ...later, same with 21.0... tired...
## Bet 20
### .1
* Added dedicated measures for hollow, Joker_Shiki (12p, Forced Declaration OFF, Weakening OFF)
* The animation system will now always restore rather than check for inequality
* Revised the Anomalocaris Killer execution conditions to allow potential persistent execution
* Anomalocaris Killer should no longer execute if the opponent can't be paused
* MoveHit will now be reset in all patterns, excluding scraping
* Manually specified that Fall.YVelocity should be 0 at all times
* Made P1StateNo Guard State Penetration the preferred method
* -3 Exploration will now repair AssertSpecial flags after execution
* During variable tampering steps, -3 Exploration will no longer penetrate variables if they would cause undesired restitution
* Polymorphic and HUD Explods should no longer be registered as visible on the opponent's side
* Stopped initial conditions from being checked during scraping
* Removed High-Priority GameTime Penetration from the full-area GameTime variable tampering pattern
* Removed some unnecessary SND data
* Fixed a memory leak issue involving the standard Gear Throw attack
* Fixed that HitDef Marking wouldn't function properly if the opponent could only be marked with ,AT attacks
* Fixed that Zayin[0]'s movecontact impersonation wouldn't be parsed properly
* Fixed that -GameTime-type substitution's value would be anomalous
* Fixed that the visuals during kfm0x05's dedicated measure would appear strange
* Fixed that phantom helpers would refer to Beatrix's address instead of the enemy address for various calculations
### .0
* **Added General-Purpose Anti-FS Countermeasures to scraping**
* Added the ability to automatically select a desired scraping target depending on the situation
* Added a 5050 return protocol to Bet[4]
* Added a quasi-general purpose measure for DarknessSevenNight
* Added a quasi-general purpose crash avoidance measure for K.Round
* Added a dedicated outtro for DarknessSevenNight (12p only)
* **General-purpose attacks no longer depend on crosstalk for proper execution**
* Adjusted the hit judgment requirements for scraping
* Revised how exploration accesses the stack
* If an anomalous circumstance is determined during scraping, certain hit conditions will be adjusted
* Revised how referencing works for projectile generation and damage calculation
* Removed the life impersonation during Phase 1 Dalet[0]
* Fixed that Frozen Life Impersonation almost never properly triggered
* Fixed that body gametime formulae would be improperly deemed invalid upon the start of roundstate = 2 
* Fixed that scraping wouldn't process hits at all if the root's movetype was A
* Fixed that damage calculation would occasionally freeze
* Fixed that scraping would revert to an previously invalidated timer if consecutive hits were parsed immediately after a timer failure
* Fixed that HitDef Marking would completely fail to register properly
* Fixed that "normal MT=H"-type abnormality sensing didn't function
* Fixed that GenericScanAX would lock up under certain circumstances
* Fixed that Body SysVar tampering would cause a crash
* Opponents with negative IDs and helpers with negative IDs should no longer cause an invalid playerID error
## Bet 19
### .3
* Allocated SND data for information storage rather than animation data
* Added a dedicated measure for Holy-Kula (12p)
* Greatly revised the storage structure of Exploration
* Greatly revised the structure of Variable Tampering
* Trimmed the air file a bit
* Fixed that scraping patterns would return a division by 0 error and erroneously extend versus some opponents
* Fixed that PrevStateNo = Instant Death/E-Youmu Killer TargetStates would occasionally return a modulus by 0 error
* Fixed that full-area Pandora Killer Variable Tampering would fail to execute properly
* Fixed that 10\*\*X random generation would only set values between 1 and 9
* Fixed that the newest version of Tim Skai's dedicated measure would also be used for older versions
* Fixed that dedicated measures would be unable to obtain Clsn1 for p1stateno callback
* Fixed that some dedicated measures would become unnaturally heavy
* Fixed that Struggle Quartet's bulk setting had an incorrect time limit
### .2
* Implemented a Time Limit Setting
* Created tournament-specific batch settings
* Added a failsafe for destroyed helpers unintentionally branching during enemy processing
* Added an undefined Ayu-Ayu state candidate
* Added a Quasi-General Purpose Variable Tampering measure for ONI-MIKO
* Life Synchronization will now take priority over all other early-game scouting options
* Inverted the directionality of variable information parsing
* Scraping will now no longer execute in a runoff-like fashion if it might register a hit in the near future
* Armor Killer will now check true life in addition to normal life to determine extension
* Armor Killer's stasis determination protocol has been slightly altered
* Enemy Helper Armor will no longer perform guard state correction on normal helpers
* Final scraping and Forced declaration of death will now refer to time in real-world seconds, rather than frames
* Slightly modified the impersonation protocol for Forced Declaration of Death
* After 400F, Forced Declaration of Death will now also throw the enemy
* Armor Killer will now check true life in addition to normal life to determine extension
* Fixed that when Aleph[0] is branched to, the subscript 0 fades out instead of in
* Fixed that unfrozen life impersonation never actually impersonated life = 0
* Fixed that damage-influenced variables that increased/decreased by 1 would be treated as timer variables
* Fixed that the Pe[X] branches weren't recorded properly
* Fixed that full-area helper sysvar tampering would sometimes cause a crash
### .1
* Added a debugging function for weight profiling, NextExplod support
* Added a dedicated measure for Creepy-Meirin
* Revised Crosstalk Helper Storage
* Crosstalk will now mostly store Explods for visual processing rather than rely on ModifyExplod
* Lightened Beatrix to the best of my current ability
	- The body will now bifurcate between a general-purpose and dedicated-measure state, depending on the situation
	- Revised the looping process for enemy variable information processing
	- Revised Remote Shatter and Sparks of the Aether's audiovisual processing to reduce lag as much as possibile
	- Crosstalk will now only execute with MT=A if it's deemed necessary
	- Slightly tweaked the helper reading process for crosstalk
	- The brain helper will now branch during its loop early if a non-existent/self helper is found
	- Deprecated the original helper branching state in favor of handling everything in -2
* Fixed various crashes upon character select and potential others during information processing
* Fixed that the MoveContact impersonation for Zayin[0] didn't work properly
* Fixed that the 12th Zayin[0] pattern wasn't properly parsed
### .0
* Added average invulnerability time processing
* Added Helper HitBy to Samekh[0]
* Added true time sensing
* Added an adaptation of the Explod Expansion Pack
* Added a dedicated measure for BVP (Lv3~4)
* Stack Parsing for Exec is now in a centralized location, rather than in the user's data
* The Intro will now end a bit earlier if the enemy dies
* **Greatly Revised Scraping Processing as a whole**
	- Damage processing will now consider the average result of the previous 4 frames when able, rather than just the most recent one
	- Greatly decreased the time limit for most scraping patterns
	- Scraping will now store damage information upon branching, regardless of lock status
	- Scraping will now only lock damage patterns if they can potentially kill within the time limit
	- Rather than starting at runoff at all times, the potential interval between attacks will be calculated upon a pattern being locked
	- The attack interval timer will now more naturally ease up/down depending on the circumstance
	- Aleph[3] will now be limited to 3 individual HitOverrideAttr combinations
	- Tweaked the time limit for Aleph[F]
    - Damage-influenced and life-synchronized variables will now be nullified if they're sufficiently out of range of their initial or potential final values
* Slightly tweaked the visuals/audio for a couple of gear-based attacks
* The crosstalk parent in position 9 will now induce palno tampering
* The enemy admin helper will now use the enemy's power pointer regardless of circumstance
* If StateDef 52 doesn't exist, certain common states will no longer be called back by P1StateNo
* Shifted Body Armor to Body HitBy in Phase 1 Aleph[2]
* Greatly revised Tetris's dedicated measure
	- Implemented (most of) the sprites made by Metis
	- There is no longer a difference with the AI for tetris-protected
	- Added the capability to rotate pieces counter-clockwise to the AI
	- When moving a piece into position, the AI will now use the rotation of that piece with the smallest probability of getting stuck in a gap of some sort
	- The AI will now change based on the speed of the falling blocks. As levels increase, the AI's tolerance towards single-line clears also increases
	- If we're going to win within a potential line clear, the AI will now set that line clear's weight to 256 (guaranteed selection)
	- Improved the slick movement capabilities of the AI. Rotation-based climbing should be more frequent now
* Fixed that Guard Flag Penetration would send opponents to Def 65535
* Fixed that opponents that take damage normally, then immediately regenerate, count as being damage for the standard amount
* Fixed that Aleph[F] probably wouldn't be branched to at all
## Bet 18
### .1
* Implemented enemy,life-type synchronization detection
* Added Life Management Projectile Generation to He[1]
* Added some Power Manipulation to Ground World Reformation
* Added a quasi-general purpose measure for Ouranos (12p)
* Added a dedicated measure for hyper giga Mizuchi (3~4p)
* Variable Tampering will now use a while loop-based system
* Helper Non-System Variable Tampering should no longer execute again if Crosstalk has already tampered with the variables of a normal helper
* Slightly revised N_Alice's dedicated measure
* Removed some breakpoints that snuck into the previous update somehow
* Fixed that damage values aren't reset properly between consecutive scraping patterns
* Fixed that Helper Variable Tampering would tamper the variables of helpers that no longer exist
* Fixed that the Helper HitDef/HitBy disabling condition for Ground World Reformation was reversed
* Fixed (probably) that on rare occasion, Ground World Reformation would look completely broken
### .0
* **Added damage-influenced variable detection**
* Added While Loop functionality (.TriggerJmp)
* Added invulnerability timer/damage cancelling timer detection
* Added dedicated measures for Genesis_Sonic (12p), Terro (!12p), Shaolon (12p)
* **Scraping patterns will now also store and iterate based on information about damage-influenced variables**
* Aleph[F] will now be branched to if a damage-influenced variable can potentially be scraped
* Increased the total number of scraping patterns stored by scraping survey's exploration from 10 to 20
* Expanded life-synchronized variables to include more patterns
* **Corresponded to SouShiki_X's reverse-dedicated "Intro dialogue"**
* Reorganized the SFF to prevent some graphical errors
* The upper-left debug text will now be cleared at the start of each round
* Fixed that an opponent's ln(SND) would crash when called by -3 Exploration
* Fixed that PrevStateNo E-Youmu Killer would return a modulus by zero error if only player helper generation states can be referenced
* Fixed that overflow-based damage correction would sometimes trigger unnecessarily
* Fixed that the overflow skip for enemies without armor never actually happens
* Fixed that some scraping damage patterns would be skipped if the opponent couldn't be paused
* Fixed Thinking Mizuchi's dedicated measure
## Bet 17
### .1
* Added MoveHit/HitPauseTime impersonation to Zayin[0] under certain conditions
* Added dedicated measures for Killer Colonel, STG Flan, UNTI-KONG, New Donald Specimens ver.A, G-Break the Targets (12p), Temmie_hOI!!!!!!, @A-Aya (Nanashino's patch 12p) 
* If the enemy seems to gain a lot of power whenever it hits an attack, invulnerability will now be exerted during scraping patterns
* If Quasi-P1StateNo is the only method of state callback, Bet[1]'s callback attempts will now include ProjMissTime
* Zayin[0] will now use Helper Projectiles only, rather than both Body and Helper
* Fixed that, if marking acquired 8 targets, then 7 targets, MUGEN would crash with an assert failure
* Fixed that Armor Penetration would fail during Gimel[1]/Pe[1]
### .0
* **Significantly revised variable tampering patterns as a whole**
* Added a neglect period to various tampering patterns during Gimel[1]
* Gimel[1] and Pe[1] will now extend tampering intervals if life decrements during certain tampering patterns
* Added a backtwirl (for human operation, hold down while backstepping)
* Revised body visual processing Explods to use BindTime = 0 where possible
* Increased the maximum distance that dashable is usable by the AI
* The AI will always use a backtwirl during Aleph[1] instead of a backstep
* The AI should no longer dash twice in a row
* The AI now calculates EdgeBodyDist using the stage's coordinates, not the camera's
* Gear-based attacks will no longer cause Beatrix to stop on a dime
* Duplicate destruction will now be ignored if a redirect helper is present for that particular helper
* Replaced pattern Gimel[0] with pattern Gimel[2]
* Neglect-type variable tampering will now extend if the opponent's life decreases below its minimum recorded value
* Prohibited Pandora Killer from backing up states during Gimel[1]
* Helper variable tampering patterns have been renamed
* Bound ChangeAnim2-type Helper Clsn2+HitOverride grant to a flag, rather than to specific patterns
* Prohibited Pandora Killer from backing up states during Gimel[1]
* Removed pattern Vav[8]
* Fixed that Backstep's animation system was not properly revised
* Fixed that Body GameTime Defense Penetration couldn't be properly performed with -3 exploration
* Fixed that variable tampering would overlap in the context of some redirect variable combinations
## Bet 16
### .3
* Added Helper 140 Loop Prevention to Crosstalk
* Added 5150 Impersonation to Tav[0]
* Added P1StateNo interference to Phase 2 Samekh[0~1] if the opponent seems to be lingering in a state for too long
* Added HPT to Forced Declaration of Death early on
* Added a quasi-general purpose measure for Valentine-Chocolate
* Added dedicated measures for Tetris(-Protected), SyanhaiXAlice (8~10p, 12p), Karma (11p)
* Revised -3 Exploration to use a clone of its target's player data
* If a normal helper would SelfState to an undefined state, it will now be deleted instead of SelfStating
* Expanded the Common State Blacklist for Instant Death states
* Limited Marking-based Guard State Interference to a flag
* Revised the detection process for Exploration-derived 110/115 blacklisting
* Ensured unfrozen Life = 0 impersonation functions properly during Forced Declaration of Death
* Revised the alive impersonation pattern for Forced Declaration of Death during RS=3
* Removed the Quasi-General Purpose measure for general Donald
* Fixed that Helper SelfState wouldn't function properly during RS < 2
* Fixed that the Constant NoKO flag wasn't set properly
* Fixed that the target acquisition bug from the last several updates persisted during the second round, and only the second round
* Fixed a mistake in Sleeping Popuko's dedicated measure
### .2
* Added Anomalous Guard State Loop Detection
* Added Helper PrevStateNo Throwing to Phase 2 Vav[1]
* Added a quasi-general purpose measure for !!!-1
* Added dedicated measures for Kashitsuki (Old Version, 12p) and Dirty KFM (STG's Patch, Difficulty Up Switch ON)
* During some Vav patterns, helpers will now additionally remain in the specified throwing state
* If an Anomalous Guard State Loop is detected or during Phase 2, Zayin[0] will use a special type of Attr variance
* Fixed that caused Beatrix would crash upon obtaining multiple targets
* Fixed that Body Armor would always be active regardless of the Body Armor Nullification flag
* Fixed that P2StateNo HitDef would execute even if another HitDef was in-progress
* Fixed that the HitFlag Nullification switch would also set Guard.Dist to 2147483647
* Fixed that the alternate Zayin[0] pattern flag is instead bound to Vav[0]
* Fixed that Scraping patterns' branching flags wouldn't be reset when entering phase 2
### .1
* Added RoundState != 2 NoKO Detection
* Added MT=H Instant Death Throwing to Samekh[1]
* Added a Vav[1] branch after Aleph[2~F]
* Added a large HPT ReversalDef to Phase 2 Bet[8]
* Added Extended Variable Tampering support (Setting-dependent)
* Revised KeepOne processing for Marking Target Acquisition
* If the enemy lingers in Beatrix's states for too long, they will now automatically return to their normal processing
* If the enemy has died, the invulnerability detection switch will be disabled
* Made Aleph[F] far more lenient in terms of continuing scraping
* Forced Declaration will no longer be branched to if RS!=2 NoKO is present
* Revised the Quasi-General Purpose measure for Wicked Law's Witch
* Limited Bet[1]'s MT=H Throwing to Phase 2
* Fixed that all attacks would have an Air.Type of High regardless of Ground.Type
* Fixed that the anti-overflow measure for helpers would unintentionally trigger
* Fixed that Body Obsfucation would never respawn a helper
* Fixed that large HPT ReversalDefs would be used prematurely during phase 1
* Fixed that the life impersonation pattern for Bet[1] was inverted
* Fixed that Guard State storage processing only worked if helpertype=player
### .0
* Added Helper SelfState execution in a lot more patterns
* Added P2StateNo Throwing Detection to the Brain Helper when HitBy/HitDef aren't active
* Added Body HitDef for a short time period during Phase 1 Bet[0]
* Added Instant Death Impersonation to Phase 2 Bet[0]
* Added Alive < 1 impersonation to Phase 2 Zayin[0]
* Added Time impersonation to Phase 2 Zayin[0] on subsequent passes
* Added Quasi-General purpose measures for ShadowSiguma2nd, 鏡餅
* Added dedicated measures for Attack or Retreat, S-Venality KFM
* **Heavily Revised Normalization Processing**
* Slightly adjusted crosstalk parent deletion processing
* Adjusted Intro Crosstalk when used against Normal Helpers
* Briefly limited Body HitDef during Aleph[0]
* If a true life variable is detected, life decrementation states will now reset
* Vav[0]'s normalization status will now depend on execution count
* Removed Helper GameTime Penetration during part of Vav[0]
* Power will now adjust depending on progression during LoC's dedicated measure
* Fixed that gears would shrink indefinitely immediately as they were spawned
* Fixed that HitPauseTime would not decrement naturally
* Fixed that Bet[5]'s PrevStateNo breakthrough formula would reset prematurely
* Fixed an infinite loop crash during Bet[E] if only Infiltrative Exploration Player Helper states were defined
* Fixed that, in rare situations, the HUD would not properly be placed
## Bet 15
### .1
* Added TargetPowerAdd support to Crosstalk under certain circumstances
* Added general-purpose Helper PalNo tampering to 1 slot
* Added a 5F limiter for ultra-instant death regardless of if a NoKO flag is present or not during Normal Instant Death Throwing
* Added a secondary execution condition for Bet[8]'s Crosstalk Ultra-Instant Death Callback
* Added Large HPT ReversalDef usage to Samekh[8] during Phase 2
* Added a power 1-gauge limit protocol to Phase 2 Zayin[0]
* Added a quasi-general purpose crash avoidance measure for B-Koakuma
* Added dedicated measures for Marisa's Head (6p), Marisa's Tree (6p), Brutal-Murderer_CLG, kagaminosekainiirumouhitorinoTohno[S], 
* An Obsfucated Body Helper will no longer die under any circumstances
* Slightly altered the leaving period extension deadline
* Slightly revised the power manipulation pattern for Aleph[2]
* Slightly adjusted the Aleph[F] branch condition
* Expanded life variable detection to no longer be sysvar-related
* Slightly optimized damage variable detection process
* Scraping will now reference life variables, if they exist, rather than life itself
* Fixed that Damage Cancelling Detection didn't work properly during Aleph[1]
* Fixed that the wrong flags were used in determining whether the enemy can be influenced or not
* Fixed that HelperType = Proj was not properly considered during helper processing
* Fixed that Letter of Challenge's first worldline would be frozen indefinitely
### .0
* **Massively Revised Scraping Processing**
* **Implemented Scraping Surveying**
* Added Patterns Tav[0], Aleph[3], Aleph[F]
* Added Quasi-Variable Tampering detection
* Added TargetVelSet GameTime Defense Penetration
* Added a limiter to Intro crosstalk if a helper continuously destroys itself and respawns
* Added Position Impersonation during Phase 2 Aleph[2]
* Added Body Variable Synchronization to Phase 2 Vav[0]
* Added a Quasi-General Purpose measure for R-sitei
* Added dedicated measures for Marisa's Shadow, Idol Azazel V2, go die man (Lv2), Faitality No.1
* Added some preset config files
* The debug HUD now has a visual difference between Phase 1 and Phase 2
* Crosstalk will now execute during Gimel patterns if Quasi-Variable Tampering is detected
* Limited Vav[8~A], Gimel[0~2] to Phase 2 only
* Fixed that initial scraping would sometimes last shorter than intended
* Fixed that Hit Detection would flag false positives during Aleph[0]
* Fixed that Random Life Detection wasn't working properly
* Fixed that Helpers would use the root's variables at time = 0 as a reference for adjustment
* Fixed that ParentVarSet processing would use the executor's variables, not the parent's variables, as a reference for adjustment
* Fixed that System Variable Tampering would incorrectly use normal variables as a reference for adjustment
## Bet 14
### .9
* Added Alive < 1 Impersonation to all Samekh patterns during Phase 2
* Addded Redirect-based/Captured Helper Clsn2 Anim Impersonation to Phase 2 Bet[1]
* Addded Redirect-based/Captured Helper Clsn1 Anim Impersonation to Phase 2 Bet[2]
* Added Quasi-General Purpose Measures for F-D-Flan, Blue
* Added Dedicated Measures for My name is Mizuchi (1p Difficulty Lv.2, 5p)
* Tweaked .AnimRevision to be more efficient
* Revised Animation and Physics Processing
* Changed the default value of Alive < 1 impersonation
* Adjusted the Life Impersonation protocol during Phase 1 Zayin[0] slightly
* Fixed that Pausing during the Intro no longer worked properly
* Fixed that Forced Declaration was never branched to under any circumstance
### .8
* Added P1StateNo Interference after 3000F has passed during the intro
* Added Common ReversalDef detection
* Added a scraping interrupt if the opponent lingers at life = 0 without HPT for too long
* Added HitDef Attr Cycling to Zayin[0] if Common ReversalDef isn't detected
* Added HPT Nullification to Phase 1 Zayin[0]
* Added Body Helper ID Variable Scanning to Redirect Processing
* Added an alternate damage granting pattern to phase 2 Bet[4]
* Added a Quasi-General Purpose measure for G-AKIHA_N
* Added Dedicated Measures for manomeguru, A_Minagi (9~10p, 12p), kfm0x05
* Slightly revised the scraping life auto-adjustment condition
* During Phase 1 Zayin[0], Beatrix's life will now always remain less than the opponent's
* Fixed that Bet[1]'s second Armor Penetration pattern wouldn't delay properly
### .7
* Added "True Armor Helper" detection
* Added Damage Storage Variable detection
* Added Name Pointer Adjustment to Crosstalk-based -3 Exploration
* Added Variable Tampering limiting for normal variables containing addresses
* Added Fall Instant Death processing to Vav[9]
* Added an Ayu-Ayu Killer pattern to Phase 2 Bet[0]
* Added power manipulation to Forced Declaration of Death
* Added Quasi-General Purpose measures for 自転車, GDPT-Challenge (12p)
* Added Dedicated measures for DDFCP (2p), Tim_SkAI, Bloody Chaos (9~12p, Defence Level 2~3)
* Adjusted the Anomalocaris Killer execution condition
* Made initial scraping start sooner if Exploration is active
* Adjusted Initial Scraping to only proceed if the "True Armor" is hit if no life decrement is detected
* Initial Scraping will now deal slightly less damage if the current damage value is stored
* Extended the length of Forced Declaration of Death initialization
* Deleted 1 crosstalk helper from general-purpose processing
* Disabled Scouting HitBy during the first half of Vav[1]
* Fixed that crosstalk would fail if certain addresses are negative
* Fixed that the enemy would crash upon entering state data if the address is negative
* Fixed that Beatrix's gears would sometimes misbehave
* Fixed a bug that caused GenericCopyAX to not copy properly if the source increment isn't set to 4
* Fixed that Helper Anomalocaris Killer would never execute
* Fixed a visual error in Direct Death by CopyPaste Dedicated Measures
* Fixed DX_Machina's Dedicated Measure
### .6
* Added 5150 callback to RS=3 processing
* Added a Precision Flow Damage Pattern to Vav[9]
* Added Frozen Life = 0 Impersonation to Phase 2 Zayin[0]
* Scraping will now exclude life = 0 in damage processing
* Removed the dedicated measure for Vesta (12p, Defense Lv.1+2)
* Fixed that -3 Exploration would never execute regardless of setting
* Fixed that Dalet[1] would not perform Frozen Life = 0 impersonation
* Fixed that Zayin[0] would cut off immediately upon branching on some occasions
* Fixed Argento's and Sacrifice-I's dedicated measures
### .5
* Added profile to the Japanese README
* Added PrevStateNo Escape detection
* Added HitOverride StateNo = StateNo Detection
* Added a Helper Life Restoration Protocol
* Added a LifeSet-induced underflow pattern to Aleph[8]
* Added PrevStateNo-type E-Youmu Killer to Vav[1]
* Added a Helper Spawning State pattern to Vav[9]
* Added Low-Life Impersonation, Helper Damage Impersonation to Vav[C]
* Added patterns Samekh[6], Samekh[7]
* Added a Quasi-General Purpose Ayu-Ayu Killer measure for Before Dying Man
* Added dedicated measures for B-kamiya (5p), WaddleDee-A (11p), Zechael-Type-S (11p,12p)
* Slightly adjusted crosstalk timing upon victory
* Adjusted -3 Exploration to include Helper Parents when executed by Crosstalk
* Slightly tweaked Life = Random detection
* Slightly tweaked Captured Helper Life Processing
* Made the Initial Scraping Projectile's Hitbox larger
* Slightly adjusted the PrevStateNo value during Dalet[0]
* Removed HitOverride during some portions of He[1]
* Sealed ReversalDef Marking during a portion of Vav[1]
* Fixed that HitByAttr detection would incorrectly treat NotHitBy SCA as valid HitByAttr
* Fixed that Samekh[1] would not deal damage while calling back states
### .4
* Added automatic Ground.Type = None HitDef Marking when the opponent is in MoveType = H
* Added an SCA,AT HitOverride to Scouting HitBy if HPT needs to be obtained or Quasi-P1StateNo is active
* Added a Fixed State flag to Overseer processing
* Added an "unsticking" flag if the opponent remains in MoveType = H too long to Aleph[1]
* Added an alternate form of ReversalDef Vampirism detection
* Added a Quasi-General Purpose Pinpoint measure for STG KFM
* Added a Quasi-General Purpose Exploration Avoidance measure for A-Michiru
* If the opponent cannot be reversed and HitDef marking is already executing, callback-type ReversalDef marking will no longer execute
* StandBy restoration will no longer occur for opponents whose StandBy started at 0
* Switched the order of overflow and underflow during initial scraping
* Extended Samekh[E] from 300F to 400F
* After a certain period of time, pausing during RS=4 will stop in periodic intervals
* Greatly increased the speed of WoPF and HellFlamer's dedicated measures
* Fixed that overflow states could accidentally be called with P1StateNo under specific circumstances
* Fixed that Bet[5] would not properly recognize life decrementation
* Fixed that Samekh[E] would not properly call back Helper Spawning States
* Fixed that Initial Scraping would consider the life modification done if it's supposed to be random as damage
* Fixed that Ultra-Instant Death Callback was only active for 1 frame at a time
### .3
* Added an ID variable tampering pattern to Gimel[1]
* Added an alternate TargetVelSet interference pattern to Dalet[5]
* Added Helper TargetLifeAdd interference to Vav[9] during the LifeMax fluctuation step
* Added Marking DEK TargetState to Vav[9] during the barabara step
* Added Life = 0, Alive < 1 Impersonation to He[1] during Phase 2
* Added a quasi-general purpose measure for 余殃の斑雪
* Added dedicated measures for D-Syaoren, Laetitia, Vesta (12p), Scene (Nameless Poporo's patch, 12p)
* Changed the static ID value for enemy helper spawning
* Fixed that Ctrl would not properly be restored after -3 Exploration
* Fixed that opponents with defence = 0 would cause the game to occasionally lag upon damage calculation
* Fixed that Life Impersonation would be inverted during Bet patterns
* Fixed that Crosstalk Variable Tampering would sometimes set using the incorrect variable
### .2
* Added Helper ReversalDef Ayu-Ayu functionality to Helper Anomolocaris Killer granting
* Added MoveType = H Callback to Forced Declaration of Death
* Added a dedicated measure for Alyssa Searrs EX
* Revised the scraping damage condition to use total damage rather than damage in intervals
* Fixed that the Aleph[1] overflow flag was errantly set if the enemy simply couldn't be hit
* Fixed that Zayin[0] would stop scraping processing if the enemy was never hit
* Fixed that MoveReversed was not properly detected during Zayin[0]
### .1
* Added a flag that marks if Aleph[1]'s scraping timer overflowed
* Added a 5100 bug avoidance switch to root processing
* Added Life=0/Alive=0 Helper Processing to Bet[3]
* Added a dedicated measure for Glacial Girl (11-12p)
* Added a dedicated measure for Transporter
* Extended Bet[3]'s common state processing to 2F at a time, not 1F
* Revised Zayin[0] into a Body HitDef scraping-esque pattern
* Revised the conditions for Alive = 0 Impersonation during Zayin[0]
* Revised 5100 bug detection
* Vav[1] will no longer set life to lifemax if it killed the helper successfully
### .0
* Revised the Variable Tampering System to execute from Overseer
* Added a new skill (Remote Shattering)
* Added a more consistent GT=None Fall Instant Death pattern to Samekh[4]
* Made AI use hypers slightly less often (1/256 -> 1/320)
* Increased the waiting time for Bet[2] OTHK from 60F to 77F
* Separated beatrix_aip.st into 2 different files
* Removed the LoC-style Explod from various dedicated measures
* Fixed that Bet[1] would branch early if no non-common Clsn2 anims were found whatsoever
* Fixed that Bet[2] would linger if a HitDef state called by it wasn't defined
* Fixed that variable restitution behaved strangely under certain circumstances
* Fixed that GameTime Defense Penetration wouldn't execute during Vav[8-B]
* Fixed that Bet[3] would call back 5100 regardless of Liedown.Time
* Fixed that P2StateNo was not properly specified during Samekh[4]
* Fixed LoC's dedicated measure
* Fixed Mainyu's dedicated measure

## Bet 13
### .0
* Added a waiting period cancellation branch to Aleph[0]
* Added a pseudo-constant invulnerability detection system (scraping will be initialized as runoff)
* Added a limiter to Marking Life Auto-adjustment
* Added a Variable Tampering pattern to Forced Declaration if WinKO is not declared
* Added a Quasi-General Purpose Exploration Ignorance measure for Athene
* Added Dedicated Measures for Orga Itsuka (11p) and Cho Kichiku Toki
* Added HitOverride to the admin helper (prevent throwing invuln)
* Allowed Fullscreen Clsn after 3000F in Aleph[0]
* Adjusted hit detection to require at least one hit on a non-dummy helper, rather than a non-normal helper
* The lower limit of common life impersonation will now auto-adjust depending on enemy,life
* Tweaked the condition for HitDef Vampirism detection slightly
* Fixed the admin helper to the center of the stage rather than the root
* Revised the P1StateNo projectile
* Revised Instant Clsn1 Acquisition to use an anim with a smaller Clsn1 rather than a larger one
* Extended the individual callback processing of Samekh[3]
* Body helpers will now become invulnerable upon Obsfucation
* If a body helper isn't obtained, during Phase 2 processing, crosstalk will automatically proc upon helper spawn
* Slightly adjusted DcX-001's dedicated measure
* Removed Clsn2 Hitboxes from some Projectile animations
* Fixed that the Initial Scraping Projectile could be guarded
* Fixed a bug that caused SysVar Life management to simultaneously be treated as both Life and LifeMax - Life

## Bet 12
### .5
* Added HitDef Desperation detection to Forced Declaration of Death
* Added an alternate protocol to Forced Declaration interference
* Improved the accuracy of .NextProj
* Fixed that Clsn2 calculation wouldn't properly align if the opponent is facing left
* Fixed that Vav[C] would be branched to regardless of whether a helper ID variable was present or not
* Fixed that Forced Declaration of Death would infinitely loop
* Fixed that Forced Declaration of Death would randomly not perform TargetLifeAdd
* Fixed Volcrz's Dedicated Measures
* Fixed EXELICA (Shooting Star Patch)'s Dedicated Measures
### .4
* Added pattern Vav[C]
* Added Helper ID Variable sensing to Crosstalk
* Added Clsn Hitbox reporting capabilities to .FetchClsn
* Added Special HitByAttr case detection
* Added ReversalDef Ayu-Ayu Killer to Bet[2] during phase 2 processing
* Added a HitDef Drainage detection system
* Added MoveType = H Impersonation to Aleph[0] if HitDef Drainage is detected
* Added Fullscreen Clsn to Aleph[0] if HitDef Drainage is detected and MoveType = H impersonation isn't valid
* Made Crosstalk exert constantly during the beginning of the intro
* Tweaked initial scraping to use a 32x32 projectile instead of a fullscreen one
* Fixed that Ayin[0] would never branch to a different pattern
* Fixed that Bet[5] would be erroneously branched to and infinitely loop
* Fixed that projectiles would sometimes fire two frames in a row during Scraping outside of the runoff pattern
### .3
* Added NoKO Detection to Enemy State Processing
* Added MoveType = I Impersonation to Bet patterns if Quasi-P1StateNo processing is required
* Allowed MoveType = A helpers to execute P1StateNo processing if Quasi-P1StateNo processing is required
* Added a Bet[E] branch to the start of phase 2 processing if a body helper isn't present
* Added a dedicated measure for igniz 2k2
* Added a dedicated intro vs. Argento
* Updated Argento's Dedicated Measures to correspond with the new version
* Throwing type will automatically switch to Ultra-Instant Death if Normal Instant Death seemingly won't kill
* Fixed a bug that caused Aleph[1] to be prematurely exited rather than swapped
* Fixed a bug that caused marking to heal the opponent during scraping unintentionally
* Fixed a bug that caused StateNo\*GameTime-type variables to not be initialized properly in -3 Exploration
### .2
* Added a Dedicated Measure for ONI-MIKO-R (newest, palno%6 = 3,4,0)
* Added a Dedicated Measure for White Reimu (12p)
* Added a Dedicated Measure for Darkness Reimu (7-9p)
* Adjusted the ending of LoC's dedicated measure
* Fixed that Projectile processing wouldn't execute if damage was 0
### .1
* Added a short Crosstalk TargetState period to the end of Dalet[0]
* Added Observed Pause Granting State storage
* Added Quasi-General Purpose Measures for will-o-the-wisp and ONI-MIKO-R
* Added a Dedicated Measure for Singualrity (7-12p)
* Made the condition for Exploration-acquired Ultra-Instant Death account for ChangeState
* Forced Normal Helper states to be called back during Phase 1 Samekh[E] execution
* Change the command for "Embers of the Aether" for the sake of futureproofing
* Fixed Pause Granting States not being called back properly during Bet[8]
* Fixed Ultra-Instant Death state callback having HPT interrupted
* Fixed Ultra-Instant Death callback errantly executing if the enemy's HPT is 1
* Fixed other overflow states would be falsely reported as Ultra-Instant Death states by Exploitative Exploration
* Fixed that anim number revision would crash on rare occasion
### .0
* **Massively revised Aleph[1]'s scraping patterns**
* **Added config.exe**
* Added True Life Inversion sensing to Aleph[0]
* Added Life Modulation Pattern storage
* Added a miniature leaving period to Aleph[1] under certain circumstances
* Added Life = 0 Impersonation to Bet[1]'s armor penetration projectile
* Added Life Decrementation State detection
* Added pattern Bet[5]
* Added a quasi-general purpose measure for Folgerichtigkeit
* Added dedicated measures for Black Core (12p), Sadist Nanaya, 283, HentaiKusoDokata, A small giant dragon (2p)
* Improved the Reverse Damage Calculation formula
* Allowed initial scraping to be extended to 25500F
* Slightly altered the conditions for RS = 1 ReversalDef
* Reorganized common HitBy callback slightly
* The AI Pattern HUD will now move depending on whether the clipboard is open or not
* Slightly reorganized the SFF to allow for proper config reading
* Deleted the CONFIG found in the def file
* Fixed a bug that caused large negative addresses to be treated as offsets in xvar/xfvar processing
* Fixed a bug that caused damage processing to be invalidated if the enemy's Data.Defence was 0
* Fixed a bug that (if it were to ever execute) caused Data.Defence = 0 to treat all damage as 2147483647 during calculation
* Fixed a bug that caused hit detection to occasionally not register properly if Exploration spawns a projectile
* Fixed Argento (1p)'s dedicated measure
* Fixed LoC's dedicated measure

## Bet 11
### .6
* Added the ChangeState suite back into Exploration
* Added ChangeState Loop Detection to Exploration
* Added a Helper State Callback delaying measure to Bet[3]/Samekh[3]
* Added a MoveType ignoring flag to helper variable tampering
* Added a freeze prevention measure to P1StateNo callback
* Added some more Forced Declaration of Death branch points
* Added a Quasi-General Purpose Body Exploitative Exploration Blacklist measure for WLW
* Added a dedicated measure for Konnahazulid
* Added a dedicated measure for odoru goenitz
* Nullified the HitCount display
* Fixed that P1StateNo Callback would be triggered unintentionally during RS = 1
* Fixed that variables wouldn't restitute properly if Helper GameTime Defense Penetration is being performed
* Fixed that variable restitution would never be performed on the first enemy helper
* Fixed EXELICA (Shooting Star Patch)'s dedicated measure
### .5
* Added a Throwing Seal flag for Marking Target Acquisition
* Added a dedicated measure for R'lyeh Text-Special (12p)
* Forced Clsn1 to be prioritized during anim acquisition
* Fixed that -3 Body Exploration using ParentVarSet wouldn't work at all
* Fixed that Instant Death throwing wouldn't work at all
* Fixed a bug that caused Ultra-Instant Death Callback to incorrectly assume the opponent has HPT
* Fixed that Body GameTime defense penetration would sometimes not work properly with default settings
* Fixed that State Controller Reading would report a false positive for Clsn1 and Clsn2
* Fixed a typo that caused Instant Clsn1 to trigger regardless of if Clsn1 is present or not
### .4
* Added a Samekh pattern branch to the end of body variable tampering patterns
* Added Redirect HelperID clearance capabilities
* Added a Quasi-General Purpose Helper Normalization Measure for WLW
* Added a limiter to automatic Helper Guard State TargetState
* Added several more entrypoints into Forced Declaration of Death patterns
* Added a special exception to RS = 3 processing during Forced Declaration of Death
* Slightly tweaked Crosstalk Parent DestroySelf timing
* Slightly adjusted early Forced Declaration of Death execution condition
* Revised Exploitative Exploration to include a custom state flag rather than temporarily replacing state code
* Moved an additional Vav[0] processing instruction before Armor Killer execution
* Fixed a typo that caused Redirect Impersonation to not function or update properly
* Fixed a bug that caused Redirect IDs to be duplicated
* Fixed a bug that caused Redirect helpers to rarely destroy themselves unintentionally
* Fixed that Helper TargetLifeAdd would be executed erroneously during Zayin[0]
* Fixed A small giant dragon's dedicated measure
* Fixed that helpers would drop their targets during RS = 3
### .3
* Added Pattern Ayin[0]
* Added a time limiter for Bet[2] processing
* Added a quasi-general purpose measure for genjitsu[s]
* Marking will now detect the opponent's death and initialize the WinKO timer
* Changed the default forced declaration of death timing from 48440F to 44440F
* Fixed that Crosstalk Ultra-Instant Death Callback would sometimes execute if HPT isn't present
* Fixed that the HitBy/HitDef helper wouldn't properly align with the enemy if their position is too large
* Fixed that the SysVar tampering flags are never properly cleared
* Fixed that Helper PalNo Tampering would unintentionally occur
### .2
* Added Pattern Dalet[5]
* Added a countermeasure for parent,anim reading vs. opponents with parent forgery
* Added Random StateNo+Anim Impersonation to Dalet[4]
* Added MoveType = I impersonation to Dalet[4], Vav[0]
* Extended Dalet[4] to 600F
* Added a Quasi-General Purpose Measure for FT-Kanade
* Added a dedicated measures listing file
* Fixed -3 Exploration not working properly for the root
* Fixed that Dalet[4] wouldn't execute TargetState properly
### .1
* Added state callback during the intro under certain conditions
* Added Ayu-Ayu Killer to Vav[9]
* Added Dedicated Measure for G-Orochi Another
* Added a Lv.X Dedicated Measure
* Added a sprite and a sound
* Shifted P2StateNo callback in Bet patterns to phase 2 processing
* Tweaked palettes slightly
* Fixed a bug that caused Ultra-Instant Death to crash vs some opponents
* Fixed that Instant Clsn1 wasn't added to the bulk settings
* Fixed that -3 Exploration would crash MUGEN upon reloading
* Fixed that certain dedicated measures wouldn't spawn fake helpers properly
* Fixed Argento's and syokupan's dedicated measures
### .0
* **Massive Revisions to the Variable Tampering System**
* **Added support for SysVar and SysFVar GameTime Defense Penetration**
* Added the ability for Overseer to modify Beatrix's HitDef and ReversalDef attributes
* During Scraping, will now auto-execute Clsn2-granting Armor Killer
* Added differentiation protocol for #.#\*gametime\*\*3 and #.#\*gametime\*gametime\*gametime
* Added MoveType modification measures to Body Variable Tampering
* Added Enemy,PalNo Impersonation to Bet[1]
* Added an Infiltrative Exploration proc'ing measure if a helper has been captured successfully
* Added a Quasi-General Purpose Exploration measure for DC-012
* Added a measure to prevent Projectiles from filling the screen during Exploration
* Added Pos Y = 0 Fixation to Aleph[1]
* Added Body Marking to Dalet[0]
* Added Guard Penetration measures to marking
* Added 4 basic techniques
* Added Dedicated Measure for winghigt-Akane-R16
* Extended the duration of !Time Defense Penetration slightly at the beginning of the match
* Modified GenericCopyAX to allow for more precise destination address writing
* Modified StCtrlCheck to allow other players to be examined
* Expanded Variable and Formula Clsn {132 Clsn1 + 244 Clsn2}
* Updated Curunir's dedicated measure
* Disabled ANY HitDef during Phase I Bet[2] execution
* Removed Crosstalk TargetState from Zayin[0]
* **Fixed that FVar GameTime Defense Penetration wouldn't work properly**
* Fixed that Bet[2] wouldn't throw the opponent properly
* Fixed that Bet[4] would last longer than necessary
* Fixed a PlayerID error during state callback
* Fixed that remote xfvar assignment would sometimes write to the wrong location
* Fixed that HitBy would be recognized improperly
* Fixed Damage Granting Armor Killer not working properly
* Fixed that initial scraping would trigger when it wasn't supposed to
* Fixed that State SysVar Anomaly detection would falsely trigger
* Fixed that Polymorph's Clsn modification didn't seem to get ported properly
* Fixed that Marking TargetLifeSet would execute erratically
* Fixed that some dedicated processing wouldn't execute properly

## Bet 10
### .1
* Added Clsn2-Granting Armor Killer
* Added specific Attr processing to Aleph[2]
* Added Air.Juggle Measures to P1StateNo Callback
* Added Simple SysVar Life, StateNo Detection
* Added SysVar = StateNo Anomalous Synchronization measures
* Refined the Obsfucation helper's initial StateNo
* Added a visual alignment measure for C-Patchouli
* Fixed Ayu-Ayu Killer once again not working properly
* Fixed that the forced continuation condition for initial scraping wouldn't actually force a clear
* Fixed a mistake that caused Samekh[E] to crash vs certain opponents
* Fixed a mistake that caused Exploitative Exploration to crash
### .0
* **Ported all %z instructions to exec instructions**
* **Moved all Bet patterns to the helper-derived system**
* **Added Samekh[0,1,3,4,8,E]**
* **Added 2nd Phase Processing**
* Added MT = A Marking capabilities
* Added automatic Normal Helper capturing measures
* Added automatic Variable Tampering Pandora Killer to state callback
* Added a measure for full-area Variable Tampering Pandora Killer
* Added Marking TargetLifeAdd support for opponents without Armor during scraping
* Added Infiltrative Exploration facilitation measures to Aleph[0]
* Added Alive < 1 impersonation to Bet[1] during the 2nd phase
* Added some NoKO Breakthrough processing
* Added a dedicated measure for White Mage
* Marking target acquisition will now also attempt to grab a target if a projectile is fired
* Shifted Observed Helper states to the brain helper
* Shifted Bet[E] to the 2nd phase
* Adjusted initial scraping to wait less if the timer expires pre-optimization
* Adjusted velocities for Fall Instant Death processing
* Separated HitDef P2StateNo from ReversalDef P2StateNo
* Deprecated Gimel[6]
* Fixed that Marking would treat itself as non-existent throughout the match
* Fixed that P2StateNo Ayu-Ayu Killer wouldn't work properly
* Fixed that the HitDef during Aleph[0] can actually hit the opponent
* Fixed that Bet[8] would be branched to even without an Ultra-Instant Death state recorded
* Fixed that Variable Tampering-assisted Redirect Impersonation wouldn't work properly
* Fixed that enemy information variable IDs wouldn't save properly
* Fixed that Direct State Controller Reading wouldn't function properly during Exploration
* Fixed that DefenceMul wouldn't be processed properly during scraping
* Fixed that Letter of Challenge's dedicated measure would crash during Timeline 0.571024
* Fixed EXELICA (Shooting Star Patch)'s Dedicated Measure

## Bet 9
### .0
* **Implemented Forced Declaration of Death** [Technique WIP]
* Added Life/Alive = 0 HitDef detection measures
* Added Alive = 0 MoveTime increase detection
* Added P2Life synchronization measures to the scouting helper
* Fixed a bug that caused Instant Death Ayu-Ayu Killer to not function properly during Bet[1]
* Prevented too many projectiles from spawning during reverse damage calculation

## Bet 8
### .11
* Added a constant invulnerability flag for Observed HitBy (Time = 1) processing
* Observed State Storage now uses stasis time rather than accessing Time directly
* Re-fixed that Aleph[1] loop I mentioned previously
* Fixed that Observed HitBy storage would occur if the HitBy timer expired naturally
### .1
* Added a Helper TargetLifeAdd mechanism to Scraping
* Added a StateNo-related Armor Avoidance measure to Scraping
* Shifted Bet[1] to a helper-based pattern system
* Slightly revised Bet[1] to adopt a mostly cyclic structure
* Changed frequencies of state callback during Bet[1]
* Fixed that a 1F delay during initial scraping would be incorrectly assumed
* Fixed that scraping might loop infinitely in rare cases
### .0
* **Greatly Revised Aleph[1]'s Scraping System**
* Added a dedicated measure for B-Reisen and Evil_Tenshi
* Added Clsn1/Clsn2 sensing to Direct State Controller Reading in Exploration
* Rerouted Projectile processing to a purely helper-based system
* Fixed that Projectile processing may loop for a longer period of time than intended
* Fixed that Exploitative Exploration doesn't read Beatrix's Config correctly
* Fixed that Exploitative Exploration didn't blacklist 110/115 properly
* Fixed that Beatrix's Intro would behave strangely against some opponents
* Fixed that Random Life Sensing would falsely be detected

## Bet 7
### .0
* Implemented Instant Clsn1 Granting [CONFIG]
* Added Ayu-Ayu Killer, Armor Penetration Gun to pattern Gimel[0]
* Added Poison-based Instant Death to pattern Gimel[1]
* Added Throwing Attempts (Marking, HitBy -> P2StateNo, ReversalDef -> P2StateNo) to pattern Gimel[2]
* Added HitBy Callback + Damage Grant to pattern He[8]
* Added Fall Instant Death to pattern He[9]
* Added Instant Death Callback (+ P2StateNo Ayu-Ayu), Helper Life Modification to pattern He[A]
* Added Helper Life Modification Measures + Armor Killer to pattern He[B]
* Added Alive = 0 Impersonation to Pattern Zayin[0]
* Changed DEK StateDef 7 -> StateDef 8
* Changed ChangeAnim 5150 -> Pseudo-ChangeState 5150 in enemy processing
* Fixed that Beatrix's Pattern HUD wouldn't have priority display
* Fixed that Beatrix's Reverse Crosstalk helpers would cause a crash during variable tampering patterns
* Fixed that Helper Float Variable Tampering would read from unintended locations
* Fixed that patterns Gimel[2] and Vav[A] would not restore variables properly
### .1
* Added .ModifySnd as a %z function
* Re-added Ayu-Ayu Killer to Pattern Bet[1]
* Added Armor Penetration Proj to Pattern Bet[1]
* Re-added Fall Instant Death to Pattern Bet[2]
* Added an exploration-based 110/115 blacklist method
* Added Inter-Exploration State Duplicate Handling
* Allowed L-Type Gear launch trajectories to be manually specified
* Tweaked AI to launch gears to slightly more often at close range
* Updated Erina-FM's Dedicated Measure
### .2
* Added an Exploitative Exploration Helper ID Variable
* Added a 5500 Exploration Blacklist condition
* Added temporary TU KO Measures
* Added a variant of low-life mode to Aleph[2]
* Added a dedicated measure for SouShiki_X
* Relocate Observed State Processing to Overseer
* Will now disable Helper Guard State DEK if a helper hasn't been in a guard state for a while
* Modified Beatrix's displayed animations to use polymorphic Clsn instead of using 2 animations
* Modified AI slightly
* Removed the loop reset protocol from Vav[1]
* Fixed that StateNo/PrevStateNo storage is written to improperly
* Fixed that Exploitative Exploration would process blacklisted animation states
* Fixed a bug that caused Beatrix to dash improperly when controlled by the player

## Bet 6
### .0
* **Greatly Revised Helper Organization** [See [attached](https://i.imgur.com/JjhGTGN.png)]
* **Introduced Multi-Target Parallel [GetHitVar(ChainID)] + Reverse Crosstalk**
* HitBy and Infiltrative Exploration have been moved to the HitDef Helper
* Exploitative Exploration has been moved to the SkyWorld Admin Helper
* ReversalDef Processing has been moved to the Brain Helper
* Shifted StateNo variable storage to Overseer
* Added beatrix_var.st [Variable Tampering only]
* Added Crosstalk Helper Identification via GetHitVar(GroundType)
* Added PrevStateNo, Time, and Anim variable scouting
* Added a limiter to Body GameTime Defense Penetration
* Implemented the -3 Exploration method for GameTime Formula Acquisition [CONFIG-dependent]
* Implemented a State Controller reading method for Exploration [CONFIG-dependent]
* Reorganized the def file's options to try and group things better
* Decreased the number of !HPT P2StateNo states stored by the body by 3
* Decreased the number of P2StateNo states stored by the HitDef helper
* Made P2StateNo storage more generalized
* Increased the number of Exploration states stored by 5 each
* Added Clsn1/Clsn2 detection to Exploration
* Added Unknown Overflow to Exploration
* Reorganized (Parent) Variable Tampering such that it no longer uses a looping protocol
* Enemy Helpers now handle Variable Tampering by themselves, if possible. Crosstalk helpers are alerted accordingly.
* Removed Ultra-Instant Death from Helper Normalization
* Changed Redirect Impersonation to be HelperID-reliant instead of PlayerID-reliant
* Allowed Redirect Impersonation to change PlayerID if a new, suitable helper appears
* Shortened the duration of Vav[B]
* Zayin[0] now branches to Aleph[2] if Variable Tampering has already been performed
* Added some credits to the README
* Actually made memo/log.txt useful again [English only]
* Fixed Gimel[2] not executing properly
* Fixed that some Helper IDs overlap with the execution flag during Dedicated Measures
* Fixed an extremely rare, allocation-related error that caused Beatrix to crash on startup
* Fixed some text descriptions
### .1
* Added a limiter to Pattern Aleph[1]
* Added a lengthening protocol to Pattern He[1]
* He[1] will now set Beatrix's life to a low value during execution
* Added ChangeAnim 5150 to Beatrix's kill state [RoundNotOver countermeasure]
* Reenabled Body Helper Obsfucation
* Fixed P1StateNo Callback failing vs certain opponents
* Fixed that Beatrix would curtsy upon victory earlier than expected
* Fixed WinKO not being parsed properly after Ultra-Instant Death
* Fixed that Beatrix wouldn't exert RoundNotOver in some human-controlled situations
### .2
* Added a 50% chance to do damage during Bet[4], when applicable
* Added P2StateNo to standard Ayu-Ayu callback
* Slightly altered PauseMoveTime stasis timing
* Removed 5 and 6 from the list of common Clsn2 animations
* Fixed an error that caused GameTime Formula Method 1 to either cause crashes or cause Beatrix to never win.
* Fixed that Helper HitDef wouldn't function properly
* Fixed pause helper spawning executing prematurely
* Fixed that Anomalocaris Killer would incorrectly execute if a ReversalDef is active
* Fixed an error involving invalid second Crosstalk IDs

## Bet 5
### .0
* **Greatly Revised AI Pattern Organization**
* **Crosstalk is now controlled by the AI Pattern during RoundState = 2**
* Added Patterns Vav[1], Zayin[0]
* Introduced a re-iteration protocol for pattern Aleph[1]
* Changed pattern Aleph[9] to Bet[4]
* Introduced a re-cycling protocol for pattern Bet[i]
* Pattern Dalet[0] now briefly uses TargetState
* Flipped the order of Gimel[0-2] and Vav[8-B] in processing
* Re-added ID Variable Tampering to Redirect Impersonation
* Added Animation Synchronization to Redirect Impersonation
* The body will now store states with HPT granted as UID states for compatability
* Fixed Redirect Helper Storage not functioning properly
* Fixed some state callback returning values from an incorrect location
* Fixed that Anomalocaris Killer would never execute
* Fixed that Ultra-Instant Death states accessed by Infiltrative Exploration were not properly stored
* Fixed Animation callback not functioning properly
* Fixed that the first Body HitBy-based instant death state would never be returned
* Fixed Beatrix executing processing multiple times/frame versus some opponents
* Fixed Beatrix freezing versus certain opponents during pause execution

## Bet 4
### .0
* **Revised Life Reversal Sensing**
* **Implemented Vampirism detection**
* Implemented a hacky Body HitDef detection system
* Reversed the priority of the Enemy Admin Helper (SkyWorld_Main)
* Changed Helper DestroySelf timing
* Disabled Scouting HitBy during most state callback
* Added a dedicated measure for Will of prison flame and Hell_Flamer
* Fixed DEK Revision not functioning at all
* Fixed Body Information Storage not functioning at all
* Fixed a bug that caused Beatrix to crash when placed in a non-P1 position
* Fixed a bug that caused Beatrix to perpetually be in a state of Auto-AI if placed in Watch mode at least once
* Fixed that some Life/Alive based dedicated measures wouldn't function properly
* Corrected some pixels in Beatrix's sprites
### .1
* A smaller version of .DEKRevision is now performed before each TargetState
* Added Observed Helper HitDef/HitBy state acquisition
* Added a dedicated measure for 6 Kinoko Reimu
* Added a couple of sprites and sounds for the sake of appearance vs. 6 Kinoko Reimu
* Changed Beatrix's Body state processing to prevent weird position/velocity-based errors
* Fixed EXELICA (Shooting Star Patch)'s dedicated measure
* Fixed Explod Translocation not functioning at all
* Fixed a bug that caused ReversalDef marking, when pseudo-sealed, to not re-activate when the opponent is using a HitDef
* Fixed that invalid player errors would sometimes occur during "Sparks of the Aether"
* Corrected some more pixels in Beatrix's sprites
### .2
* Added a quasi-general purpose measure for Chibi Elque Challenge (2p)
* Added an Exploration Error avoidance checksum
* Modified Exploration to begin during RoundState = 2
* Modified Exploitative Exploration to run backwards
* Modified the distance calculation algorithm such that Beatrix doesn't enter a gear throwing loop
* Lengthened Pattern Gimel[1] by 10 frames
* Fixed a bug that caused pattern Gimel[0] to get stuck after writing -2147483648
* Fixed a bug that caused storage of the enemy's Var(0) to never occur
* Deleted the Long Forced Declaration of Death config option
* Deleted Def 889

## Bet 3
### .0
* **Greatly revised GameTime Formula Acquisition**
* Added several more Variable Storage animations to facilitate GameTime Formula Acquisition
* Added Variable Storage capabilities to Polymorphic animations
* Shifted some files around such that separation is easier
* Separated Beatrix into 2 parts (GetUploader-specific change)
* Fixed a bug that caused Sparks of the Aether to sometimes behave incorrectly
* Fixed a bug that caused the pause helper to spawn whenever the opponent spawned a new helper
* Fixed a bug that caused helper IDs to increase exponentially when a pause helper is spawned
* Fixed a bug that caused helpers to escape being custom stated during pattern He[1]
* Slightly changed the DBO checksum and DisplayName syntax
* Slightly changed Beatrix's name

## Bet 2
### .0
* **Began work on a scraping recognition system**
	* For now, it can detect life decrementation via damage and store optimal Damage/Poison conditions
* Revised TimerFreeze to only execute when the match is about to end via TU (KO Declaration via TU TBA at a later date)
* Fixed a bug that caused OnTop Explod initialization to occur twice
* Make the HUD look a bit nicer
* Added dedicated measures for Thinking Mizuchi and Dark-Alice 12p
### .1
* Fixed a critical error that caused the DBO to not launch without administrative privileges
* Added a pattern for Variable Tampering-based Pandora Killer (Gimel[6])
* Added a pattern for Helper SysVar Tampering (Vav[B])
* Re-implemented Basic Redirect Impersonation (First, Last, Armor | Misc. Helpers)
* Fixed a bug that caused Alive = -7 impersonation to not work during Pattern He 0
* Added TargetLifeAdd (LifeMax) + TargetState to Pattern Dalet 4
* Allocated Crosstalk Variable Preservation Animations
	* Will be used in the future for GameTime Defense Penetration
* Refactored patterns Gimel[0-2] to use a non-looping system
* Corrected Mainyu's dedicated measure to work as well as it can, as of ver. 0.75
### .2
* 

## Bet 1
### .0
* **Revised XVar to be able to remotely read/write addresses (Self available via Offset^-1)**
* Reduced the amount of %z execution by a factor of 3 in favor of XVar read/write
* (These are very big changes, even though they aren't much on the surface)
* Added the ability to toggle the Debug HUD within Preload settings
* Captured enemy helpers will automatically destroy themselves if they're a duplicate and (most likely) not useful
### .1
* Infiltrative Exploration HitBy/HitDef will now only activate if no other HitBy/HitDefs are active
* If Infiltrative Exploration isn't active, but Beatrix/her other helpers have entered a custom state, Infiltrative Explroation will inherit the custom state
* Tweaked Scouting HitBy/HitDef to not be active if the body has an active HitBy/HitDef
* Disabled Scouting HitDef during pattern Aleph 1
### .2
* Fixed a conversion mistake that caused NumTarget > 1 TargetDrop to crash MUGEN
* Fixed a bug that caused 0-Element Animations to be read improperly, causing a crash upon Projectile usage vs certain opponents
### .3
* Revised StateNo variable acquisition
* Revised Damage(-Influenced) variable acquisition
* Fixed that Beatrix would falsely believe Marking succeeded
* Modified Pattern Aleph 9 to utilize Armor Penetrating Projectiles more consistently

## Bet 0
### .0
* **Bundle Callback/Execution Procedures with AI Patterns**
* True README Added
* Tweak AI Pattern timing
* Refactor State Callback Processing
* Implemented helper information dereferencing
* Made Pause/SuperMoveTime positive again
* Moved Const(Size.Attack.Dist) -> Const(Size.Z.Width)
* Fixed a bug that caused Body Variable Tampering to not work properly
* Additionally localized Body Variable Tampering values to Beatrix's body
* Fixed 1 dedicated measure involving Guard.Dist
* Defined the Long Forced Declaration of Death config setting (unimplemented)
* Defined the Body Exploration Impersonation config setting (unimplemented)
### .1
* Added non-marking Anomalocaris Killer
* Changed Anomalocaris Killer to allow attacks to connect if HitPauseTime could be obtained by the enemy
* Fixed Enemy HitDef/HitBy recognition
* Fixed Crosstalk Ultra-Instant Death not executing properly
* Made Pattern Bet 8 last a longer period of time
* Allowed Pattern Bet 8 to call back HitDef if a HitPauseTime acquisition state isn't found
### .2
* Implemented Helper Variable Tampering AI patterns (Vav 8, Vav 9, Vav A). SysVar to be added at a later date
* Defined StateDef 1000077006 (Crosstalk Helper Variable Tampering)
* If a Bug Helper can't spawn, but our crosstalk is currently occupying needed space, we'll now delete the 6th crosstalk parent (Helper 0D) to spawn it
* If a Bug Helper can't spawn due to helper occupancy, SelfState allowance will be disabled
* Modified Armor Killer, changed AI pattern He 1
* Re-added Target Determination to HitDef/Projectiles (fixes a Debug Warning)
* Slightly clarified the sprite/cutin usage term in the README
### .3
* Fixed a bug that caused %z Marking TargetDrop to crash MUGEN (assert failure in array.c line 154) if executed
* Fixed a bug that caused a permanent freeze error during damage calculation versus opponents with Data.Defence values of 40, 20, 10, or 5
* Revised damage calculation formula to use fdiv (the extra clock cycles are worth it compared to executing the instruction more times than needed)
### .4
* Fixed that Beatrix had an identity crisis if she saw herself, causing MUGEN to crash
* Allocated 3 additional Polymorph animations for the sake of multi-Beatrix compatability
* Condensed the Exploration State Checksum for the sake of efficiency
* Performed Preparatory Rites
### .41
* Fixed that loading 2 separate instances of Beatrix (i.e. foldername = !3e^+&!x and foldername = Beatrix-Repha-master) crashed MUGEN
* Modified bootstrap.efi to be refreshed whenever Beatrix is reloaded

# ver. א (testing/initialization phase)
## Aleph 41
### .0
* **Implement Exploitative Exploration** (accessing this information TBA in 41.1)
* Add a high-priority flag to the root for easy distinguishing of MoveType = A processing
* Fixed a bug that caused Explods upon death in Beatrix's states to not be scaled properly
* Fixed several bugs involving Infiltrative Exploration
* Deprecated .ExpVarCompare, replaced with the more useful .CompareRange
* Execute Target-based Persistent Reset upon TargetState (first 4 StCtrls only, basically does nothing but guarantee a smooth transition)
* Enemy SkyWorld_Main (ID = -244777244) helpers will try to move to helper slots with higher priority, if able
### .1
* Revise Exploitative Exploration to be Self-Contained
* Recognize Exploitative Exploration as a potential target for callback
* Revise Pause protocol to not freeze opponents by accident
* Fix crashes caused by Infiltrative Exploration ChangeAnim2'ing to an invalid animation
* (Mostly) Correct Metamon dedicated measure
* Allocate option for Body Impersonation Exploration Passthrough

## Aleph 40
### .0
* **Revise Helper Space (Lhapiyel -> Heyvenyevt)**
* M'ebed Self-Marking MoveType ≠ A, Heyvenyevt Self-Marking MoveType = A
* Damage Processing -> Heyvenyevt
* Enemy Anim Processing -> Mashgyha
* Helper Information Processing -> M'ebed
* Revised Enemy Variable Tampering to use either Mashgyha or Heyvenyevt, depending on the root's priority
* Some hopeful optimizations

## Aleph 39
### .0
* **Removed Foldername Restriction**
* Fix some more Exploration-related crashes
* Actually add Time-Granting Unfrozen Instant Death to some Preload settings

## Aleph 38
### .2
* Fixed a bug that caused a permanent Pause during the round
* Fixed various Persistent-related crashes
* Fixed a freeze involving Marking TargetState
* Refined Marking Target Acquisition
### .1
* Revised Body and Helper Information Storage
* Added an Anomalocaris Killer Execution check to Body HitDef reading
* Fixed an infinite pattern loop in Bet E
* Added Heaven's_Gate Position Tracking
### .0
* **First public release** 
* **Added AI**
* Repaired pattern-based loops
* Fully implemented Patterns He 0, Dalet 4
* Added some sound effects
* Made various processing changes involving variable storage and helper priority
* Doubled self-marking for extremely consistent Life = 0 Impersonation
* There's probably various other things

## Aleph 37
### .0
* Revised %z to read from the bootstrap rather than having to define text code every single time
* Added %z Databank to the memo folder
* Added (Redirect Impersonation) Armor Killer
* Added 5150 Callback during RoundState = 3
* Implemented Marking-related Patterns (Dalet 0, 1)
* Initialized other Patterns (Bet 3, Dalet 2)
* Revised main pattern processing substantially
* Moved pause handling to Marking Helper in preparation for the future
* Fixed Ayu-Ayu Killer triggering during pattern Aleph 0
* There's probably some other things

## Aleph 36
### .3
* Implemented Redirect Helper Spawning
* Fixed that Body Variable Tampering wasn't working properly
* Fixed that Crosstalk Ultra-Instant Death Callback wasn't working properly
* Added some sprites in preparation for Forced Declaration of Death
* Added TargetLifeAdd-related processing for Forced Declaration of Death
* Defined some Forced Declaration of Death patterns
* Defined Armor Killer Pattern (He 1)
### .2
* Fixed a bug that caused XVar assignment to have incorrect operand priority (Priority 15 > Priority 7)
* Fixed a bug that caused Beatrix's palette to become Devious Distortion Deity's while transitioning between them
* Corrected a modulus by 0 error in some State Callback Processing
* Now using this new method of git pushing called "commit things as they happen you dongus"
* Lots of README-related changes
### .1
* Defined a couple Impersonation-related patterns (He 0, 8)
* Fixed a bug that caused any character loaded after Beatrix's DBO to crash upon a float comparison involving a (] or () range
* Various other XVar-related bug-fixes in Beatrix's processing
* Trimmed off a hundred or so KB by removing an unneeded && operand from DEK
* Updated SelfID code to ignore TeamSide
* Removed the DarknessSevenNight question from the README as it's no longer relevant
### .01
* Fixed a gaping design flaw that caused NumExplod(X) to read XVar(X)'s data, and NumTarget(X) to read XFVar(X)'s data
### .0
* GREAT ACHIEVEMENT.
* Revised Extended Variable Storage to use XVar/XFVar rather than Var/FVar (will probably need to fix some bugs later)
* Modified bootstrap.efi to be more efficient in terms of code loading (Checksum TBA)

## Aleph 35
### .4
* Implemented Body Variable Tampering into main AI loop
* Fixed a bug that caused all enemy entry StateNos are registered as 1 (Fall.Kill > Fall.Recover)
### .3
* Moved DBO loop code into sky/bootstrap.efi in preparation for later compatability updates
* Renamed beatrix.st to beatrix_efi.st for clarification
* Added some description
### .2
* Implemented Forced Infiltrative Exploration (Config-Dependent)
* Implemented Time-Granting Unfrozen Ultra-Instant Death (Config-Dependent)
* Moved Def -244 > Def 244243998 (Ultra-Instant Death + Helper Normalization)
* Added Def 244243997 (Ultra-Instant Death + PalNo Tampering)
* Added a TargetState to try and force opponents to leave these states if they end up in them by accident
* Added Def -333, -334 (Enemy StateDef -3 Exploration), purposeless as of now
### .1
* Implemented Body GameTime Defense Breakthrough
* Implemented PalNo Tampering Exploration (for later purposes)
* Fixed a bug that caused Exploration to not read the enemy's last StateDef
* Allowed Config to actually be accessible (abandoned Configger)
### .0
* Another big one
* Changed method of HPT Instant Death verification
* Implemented Force HitDef
* Implemented Patterns Aleph 9, Bet 0, 1, 2, 8
* Fixed a bug that caused Force HitBy to behave erratically
* Fixed a bug that caused Exploration-Based Ultra-Instant Death Callback to call StateDef 0 instead of its intended value
* Fixed a bug that caused Body GameTime Formula Acquisition to not work properly
* Granted 2 frames of HitPauseTime to Beatrix when HitDef-based callback is used
* Streamlined Random Callback selection-related procedures
* Deleted some old debugging sprites
* Updated README/bio

## Aleph 34
### .2
* Let helpers see each other's positions in memory via Alive ( = [-63, -5])
* Revised Helper Projectile Processing to have the first helper we own take ownership of the projectile instead of the user (not that it matters)
* Made several (blacklist Projectiles, prevent Anomalocaris Killer HitBy = ,NP from interfering, etc.) bugfixes for Ayu-Ayu Killer
* Fixed a bug that caused MUGEN to freeze when Beatrix used any form of HitDef or a body-based Projectile
* Cleaned unnecessary Animations
### .1
* Corrected Anomalocaris Killer to allow for marking to be done even with HitOnce
* (This is the only change, but it's a very important one)
### .0
* Achieved one of the 2 sex numbers, which is very epic
* Wanted to make it a bigger update, so it took longer ('.w.')
* LoC Dedicated Measure corrected
* Implemented Observed Clsn1+HitDef, Observed Clsn2+HitBy state addition
* Created Pattern Bet (Subpatterns 0, 1, 2, 8)
* Fixed a bug with Clsn hitbox detection that caused it to be 1 frame late (it's more like a MUGEN quirk that I don't like...)
* Fixed a similar bug with Animation Polymorphism that caused the intended image to be 1 frame late
* HUD Refabrication (37.5% scaling)
	* "BEATRIX" font: White Rabbit, 24pt, 1px OL + 1px BL
	* Hebrew Pattern font: Miriam Mono CLM Bold, 72pt, 2px OL + 2px BL
	* Hebrew Subpattern font: Miriam Mono CLM Bold, 56pt, 2px OL + 2px BL, 50% additional scaling
	* Dedicated Measures font: Migu 2M Bold, 72pt, 2px OL + 2px BL, 62.5% additional scaling
	* "Weapon" Name font (not implemented): Miriam Mono CLM Bold, 48pt, 3px OL + 1px BL, 50% additional scaling
	* Work will be finalized at a later date

## Aleph 33
### .4
* Added dedicated measure for OUT OF THIS DIMENSION (2p, TeamSide = 1 only)
* Deleted unnecessary animations
* Assured the reader that the next update will not be a subupdate for sure this time
* Began work on Extended Variable Space MKII
### .3
* Set IntPersistIndex and FloatPersistIndex to 0 (who would've guessed that this tells you what variables would be reset?)
* Fixed a bug that caused Beatrix's root to be unable to record Instant Death states
* Fixed a bug with Pattern Aleph 8 that caused Instant Death states to not be called properly
* Fixed a bug that caused Beatrix's appearance to not reset upon the round being reset (Note to self: FVar assignment chains don't work)
### .2
* Fixed a bug that caused Ayu-Ayu Killer to not work properly vs opponents without HitOverride
* Allowed P2StateNo < 0 in HitDef + ReversalDef (it's possible to do this via Null Overflow; Projectiles will not be included for the opposite reason)
* Incorporated P2StateNo < 0 as a valid callback target
* Fixed a potential bug that would cause an error for P2StateNo < 0
### .1
* Revised Pattern Aleph 8
* Implemented Marking Execution Variables (7016 = TargetState, 7017 = TargetLifeAdd, 7018 = TargetPowerAdd)
* Moved P2StateNo Variable Location
* Fixed a bug that caused some State Callback Acquisition methods to fail
* Fixed a bug that caused P1StateNo Projectiles to not be spawned if a helper wanted them to
### .0
* Corrected bug with animation finding (at long last... it should be fully fixed)
* Prepared RoundState != 2 Patterns for loading
* Implemented Pattern Aleph 8 (Overflow/Underflow Damage)
* Revised dedicated measure for Argento
* Fixed a bug where crosstalk helpers were behaving oddly upon being required for some attacks
* This should also fix a bug related to enemy processing

## Aleph 32
### .2
* Guaranteed SelfAnimExist(Random) will be satisfied
* Allowed Anim Impersonation to have range over all positive integers (future use)
* Prepared RoundState != 2 Patterns for loading
### .1
* Added dedicated measure for 食パンに乗ったぬこ (syokupan)
### .0
* Corrected the freezing error described in Aleph 31.1
* Reinstated Pattern Processing
* Added dedicated measure for Nemesis Nike (Boss Mode + God Mode, TeamSide = 1 only, EASY~Infinity)

## Aleph 31
### .1
* Incorporated Palette Remapping (Manually)
* Identified a bug in which Beatrix consistently freezes upon Aleph 0's transition (Aleph 1, Bet 0)
* Temporarily disabled pattern processing as a result of the above bug.
### .0
* Remapped Main Palette (Separated sclera from clothing (hammer will be separated from clothing at a later date))
* Created 3 Subpalettes (1 for each visual mode/"weapon")
* Created Palette Processing State (Will be used for effect (palette) processing later)

## Aleph 30
### .2
* Created Crosstalk Misc. Variable Scouting StateDef
* Moved Crosstalk ParentVarSet StateDef
* Implemented early variant of Helper Damage Variable scouting
* Allocated Damage, Fall Damage as potential special terms
### .1
* Implemented GetHit detection for helpers
* Temporarily disabled Helper ID Variable searching
### .0
* Changed Self-ID parameter to be more unique between each player
* Lengthened pattern Aleph0 processing time
* Introduced Body HitDef (HitFlag = NULL) to pattern Aleph0
* Refined Metamon countermeasures (it still crashes because of Metamon's processing)

~~there was too much to list before Aleph 30.0 that i didn't write anywhere lol~~
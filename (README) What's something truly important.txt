There's always something above something else.
The universe is no exception. Worlds transcend others in a pillar-like hierarchy... except for one.
This world is so high up, it can be fallen into from above, almost as if an integer underflow happened in the dimension itself.
The world above all, the Sky world. Its location makes it Heaven, but its true nature makes it Hell.
A world of required sacrifice for those that fall into it by accident for the first time.
But with this great sacrifice, comes great power.
6 people "reside" in this world. Each with something important to them taken away.

And the first has entered the largest nexus of the multiverse.

=================================================
Beatrix Repha
ver. ב_‎026.40 (July 14, 2022)
by Choon_2i
=================================================
Kinda table of contents:

Stuff that you'd probably be interested in
	Things that are really important to get out of the way immediately (terms of use, etc.)
	Spoilers: Reuploading/editing are banned, use common sense
Design Philosophy
	My personal reason for why I made Beatrix the way she has been made.
exec+CMDOF code summary
	Heinous Techniques™ that Beatrix uses in her coding.
On the topic of CMDOF usage
	I explain why I personally believe Beatrix is still a god character despite using the CMDOF exploit.
Issues that might pop up
	Self-explanatory, but there are always some issues...
Directory Listing
	What are the files lmao
Technique Listing
	For the brave souls that actually want to play as Beatrix, despite her being unplayable in the slightest, so you can pretend you beat that one cheap Touhou edit that bothered you for like 5 million years.
Y-Mode
	More information about this specific setting
Something something bio
	Click the link tbh
Special Thanks
	A requirement, but there are some people that I legitimately feel thankful towards.

=============================================
Stuff that you'd probably be interested in
=============================================
This character is designed to work in WinMUGEN only, as it utilizes the Command Overflow (CMDOF) exploit.
I don't guarantee that Beatrix will work in any other MUGEN environment.
Please don't try to port this character to said other versions, either.
I aimed to make Beatrix approximately an upper-level god character (known here in the west as a nuke), despite the aforementioned exploit.

If the CMDOF exploit isn't executed, this character will most likely crash.
Sorry but, unless an alternative method is found, it can't be helped _(:3」∠)_
You can load this character before picking said other ones to get around the issue, though.

You can adjust the options or even the portrait using config.exe. Explanations are in tooltips.
	Since it's a bit unstable under wine, but still usable, I might make a mac version if enough people want it.
This character's theme is 月下美人, a free-to-use track by DOVA-SYNDROME.
	There's no way to add a toggle without filesize bloat, so... grab the SND file from an old version if you want this?
Lastly, if a yukkuri's on screen, either weebl's stockmarket (dedicated measures) or an extended version of deltarune's cool mixtape (Y-mode) will play.

The below are my mugen.cfg settings.
LayeredSpriteMax = 10000
ExplodMax = 2048
SysExplodMax = 48
HelperMax = 56
PlayerProjectileMax = 512
Feel free to use these as a reference when using this character.
Be careful when increasing ExplodMax, as certain characters lag heavily depending on this setting (NumExplod-related)

-------------

Editing this character, whether for the purpose of claiming it as your own, copy-pasting code, or otherwise, is strictly forbidden.
Commercial usage of this character or any parts of her is also prohibited for obvious reasons.
You are more than welcome to use it as a reference, though!
If you want to tinker with it for the sake of testing, please let me know in advance. I'll probably end up testing it for you instead.
Same deal with any sort of patch. Usually it's something that I should be implementing myself anyway.

For your sake, be very careful when opening beatrix_efi.st.
This file contains the CMDOF exploit, and saving the file in the wrong text editor may cause MUGEN to crash.
This includes FF3's text editor, so you should immediately close the file when opening this character in Fighter Factory/etc.
There is a backup of beatrix_efi.st that you can restore from if needed, but still, be careful.
If you really want to look at it, try opening it in the Sakura editor.

Please let me know in advance before using this character's graphics or audio.
While I didn't make everything used, I would still like to know what you're using them for.
Using resources such as the select portrait and cutins of this character in other characters is strictly prohibited.
To be more specific:
	SFF Group 7XXX: You should probably ask me
	SFF Group 8XXX: DO NOT USE
	SFF Entry 9000,1: DO NOT USE
	Anything else: should be ok
You CAN use the cutins/select portraits/theme in videos, so long as appropriate credit is given!
	It's preferable that you find the original resouces yourself since they're going to be higher quality; I usually credit my sources!
If you're unsure, ask.

Please use common sense when using anything from this character, though.
Odds are, if you're making a spriteswap or something like that, it isn't going to be good.
In other words, if you wouldn't like me to do the same thing to your stuff, you probably shouldn't do it yourself!

-------------

Video and tournament usage is perfectly acceptable!
It goes without saying that fanart is as well. Please show me if you do make something with her so I can look at it.
It's best to ask me in advance, of course, but 90% of the time, I'll say "yeah sure go ahead!!"

My personal request is that she doesn't appear in any sort of "bashing" videos, though.
I personally believe that these sorts of videos don't contribute anything of importance and Beatrix wouldn't be involved in such things.
For similar reasons, don't include her in any "controversial" works either (i.e. politically charged works).

There's only 1 selectable palette for this character: 7p. Not even 1p is selectable.
You may either indicate this as 7p or simply omit it (or list it as "no palette difference" in the case of some tournaments).
Additionally, Beatrix is designed for fighting only one enemy, as is standard for most god characters.
Putting her in a battle with 2 enemies will result in an immediate error.
Similarly, skipping the intro will ALSO result in an immediate error. 

Despite Mashiro having her own voice, Beatrix herself is completely silent, and the voice files are removed.
This is intentional. Adding a voice to her would ruin her concept, so please don't do that.
Since I've added the profile, read that if you want to learn why.

-------------

If you'd like to share this character, you may link back to the below two places.
Re-uploading this character is forbidden as long as at least one of the below links are valid.

https://ux.getuploader.com/choon_2i/
	• Major version updates only, might be slightly outdated. Does not contain old versions.
	• This is the site I'll personally use to notify people on twitter through the #MUGEN更新情報 hashtag.
	• Password to download is |לעלות לשמים|, which is also posted on the website for easy copy-pasting purposes.

https://gitlab.com/Pikachuun/Beatrix-Repha/
	• Whenever I feel like an update was made (semi-often). Most up-to-date, contains old versions because git does that automatically.
	• It's important to note that these versions may not be stable. While no version has this guarantee, this applies to versions found here the most.
	• Beatrix's dedicated twitter (@GearB0rf) will only use gitlab links.

If you see her anywhere besides these places, and these places are still valid, let me know and try to get it removed.
Most likely, it will only be reuploaded at one particular location that I don't really support the practices of... (´･ω･`)

You may report bugs or request features here.
vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
https://gitlab.com/Pikachuun/Beatrix-Repha/issues
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
(You may need a gitlab account, but it's easy to set one up)
Or just ping me on twitter. One of the two.

=============================================
Design Philosophy
=============================================
What truly qualifies as a god character?
Is it not using clipboard exploits?
	If so, why is Witch Angeline, Erina-FM, etc. a god character?
Is it not modifying the engine?
	If so, why is Forbidden Knowledge a god character?
Is it not using the kernel calls to do anything?
	If so, why is skeleton's G-Mizuka patch a god character?
Is it not modifying the opponent "directly?"
	Which seems the most sensible.

As a result, excluding modifying the opponent's data (outside of our states), I'm assuming that I can do whatever I want to.
Whether direct (through something like %n, buffer overflows, etc.) or indirect (through something like modifying the engine).
Basically, if a god character could theoretically do it judging by prior knowledge, or has already done it, so can I.

If you're skeptical about this, I understand. The best I can do is provide the disassembled exec code immediately.
I can also provide the disassembled CMDOF code for clarification purposes, if you really need it.

In terms of general design, though, I wanted to make a heavily story-based god character.
Beatrix existed before this edit was made and aspects of her have been fleshed out beforehand.
I wanted to take this sort of concept and try to place it in MUGEN's environment.
Simply put, I didn't want to make a god edit, like everyone else.
Instead, I wanted to make a god character.
She's still not complete in this respect, though... give it time.

=============================================
exec+CMDOF code summary
=============================================
If something is listed below, but isn't implemented in Beatrix yet, it means it will be at some point.
Knowing me, I probably listed everything I wanted to do before actually doing anything...

------------------------
Really Powerful Tech
------------------------
**Extended Variable Space**
	Beatrix will have access to 1024 additional variables in total for each player, labeled XVar(0~511) and XFVar(0~511).
	These variables cannot be modified through ParentVarSet.
	This assignment can also be used to assign to self data (xvar(offset^-1)) and remote locations (xvar(POINTER)), similar to var(address).
**Null Over-no**
	Beatrix does not use Null Overflow states for any purpose.
	Ultra-Instant Death, PalNo Tampering, and Normalization are handled by xvar assignment if the usual conditions of null overflow are met.
	As you'd expect, Beatrix uses xvar assignment for everything related to her own data. 
	For the enemy, only the following parameters are modified with these methods in an anomalous fashion, and only when the enemy is reading our data:
		• GetHitVar(Fall.RecoverTime)
		• GetHitVar(Fall.EnvShake.Phase)
		• StateNo
		• PrevStateNo
		• Time
		• StateType=0 (for variable tampering priority determination)
		• Ctrl (!=[0,1])
	Normal Helpers are immune to this limitation, excluding things like consts/etc. that can't be modified anyway.
	I don't think these would make any major difference, and most of them are restored later to their original values, hence their selection.
**Fake Player Generation/Code Execution**
	For the sake of techs like Exploration, a fake player structure, including a fake parent and root, is generated.
	State code is then executed on a controller-by-controller basis, free from crashing in most circumstances.
	Unlike past versions, there's no cloning of state data, and no modification/nullification therein.
		However, Explods/Projectiles will have no effect depending on if a flag is set or not.
	Important data is restored at the end of processing, such as the MUGEN information structure, all player variables/states, etc.
**Exploration Body Impersonation**
	If the corresponding CONFIG option is enabled, Beatrix will set the isHelper flag to 0 during exploration.
	This allows body-related data to be read, instead of helper-related data.
	Because of the possibility for enemy-related checksums to break, a second passthrough will be performed to obtain helper data.
**Explod Translocation (NumExplod Impersonation)**
	Beatrix will, except in certain circumstances, set her own Explods' parentID to an inverted version of her own ID.
	The result is that Beatrix will seemingly have a NumExplod = 0 at all times.
	This isn't really *that* strong, since you could just use that helper for visual processing instead, but is noted here anyway.
**Reverse Damage Calculation**
	Beatrix will obtain Projectile Damage values from a desired damage value using code execution.
	Used for scraping purposes, damage overflow/underflow, etc.
	I could just change the enemy's defence multiplier to 1 like a certain other character (EoE, but EoE changes Data.Defence directly) but that isn't fun
**Phantom Helpers (NumHelper Impersonation)**
	By detaching a helper, Beatrix can reduce her own NumHelper count by 1. This process can be repeated as many times as needed.
	This is only done in 2 cases.
		• Some Dedicated Measures
		• Projectile Masking (Scraping; in this case, it's always accompanied by something else)
**RNG Manipulation**
	Beatrix can directly modify the seed of MUGEN's LCRNG algorithm as she sees fit.
	Setting-dependent.

------------------------
Code Assistance-Related Tech
------------------------
Variable Restitution
	Beatrix will backup and restore variables as necessary.
Enhanced StateNo Defense Breakthrough
	Beatrix will copy the enemy's StateDef #s and use them herself.
	In addition, every frame or upon marking, Beatrix will also copy all players' StateNo/PrevStateNo values into a separate buffer
Enhanced Anim Defense Breakthrough
	Before ChangeAnim2 is called, Beatrix will copy the Anim # of whatever she captured and store it in her own animation data.
Self-Contained Anim Acquisition
	Beatrix will obtain the enemy's animation data directly and iterate through it in order to obtain information (Clsn1/Clsn2).
Spwaned Helper ID Modification
	When the enemy admin helper is generated, it will be statically defined.
	This is merely done for easy access.
		(>'.3.')> Administrator: 16244000 + (EnemyID&255)
		(>'.3.')> Loc@lhost: 2144244000 + (EnemyID&255)
Direct Reads of various hard-to-obtain parameters
	HitDefAttr
	HitByAttr
	HitOverrideAttr
	Clsn
	HelperID
	etc.
Probably some more things that I missed

------------------------
General God Character Things
------------------------
Probably, not everything here is actually implemented.
But I am at least planning on implementing it, or have everything ready to implement, so I'm listing it here anyway.
(*Config-Dependent Execution)

The body does most things, so it looks normal except for when it doesn't
	It displays animations
	It uses HitDef most of the time
	It also gets hit with/without armor
	And Time increases as it normally would
	While it does use states for its processing, they don't really matter
		I could move everything to Def -2/-1... but nah
	We're ignoring both the Life and Power gauge for its techniques
Ignore the effects of Pause/SuperPause while exerting it on command
KO'ing the opponent/herself is the only way to end the round
	Except in some circumstances (Ouranos)
Precision damage + Scraping
	Overflow Damage (2147483647, etc.)
	Underflow Damage (-2147483648, etc.)
Opening Parallel Crosstalk
	Full-Area (&255 ≥ 128) Parent-Change
	StateNo Defense Breakthrough (Devil's Eye Killer)
		Guard State Bypass
	Helper Variable Tampering
	Helper GameTime Defense Penetration
Once we capture a helper, we can do
	Normal Helper Spawn
		It has a negative helper ID to limit unintended interference
	Exploitative Exploration
	(Unfrozen) Helper Normalization
		* Time-Granting Normalization
	Helper Sys(F)Var Tampering
	Armor Killer
	Redirect Impersonation
	Crosstalk-Related State Callback
	Helper PalNo Tampering
If the helper's parent is the enemy body, we can also do
	Body Variable Tampering
		Damage, Life, StateNo (Pandora Killer Reform), and Helper ID Variable tampering
	Body Variable Restoration
	Body GameTime Defense Penetration
If we somehow get the enemy in these states, we can additionally do
	Instant Death
	(Unfrozen) Ultra-Instant Death
		* Time-Granting Ultra-Instant Death
	Body Sys(F)Var Tampering
	Body PalNo Tampering
		* Time-Granting Body PalNo Tampering
Anomalocaris Killer
!Time Defense Breakthrough
Marking
	We can probably TargetState whenever
		Body Devil's Eye Killer too
	TargetLifeAdd (Poison)
	Power Tampering is rarely useful, but is done anyways
Infiltrative Exploration
State Callback (Projectile/HitDef, ReversalDef, and Crosstalk are used depending on the situation; Observed and Exploration-derived states are both supported)
	Ground.Type = None, Trip usage
	Instant Death Callback (G-Orochi, F1, etc.)
	Ultra Instant Death Callback
	OTHK (HitDef Callback)
	Pandora Killer (HitBy Callback)
	Hurt State Callback
	E-Youmu Killer (Helper-spawning State Callback)
	Clsn1+Clsn2 Anim Callback
	Common Callback
	Helper State Callback
	ForceStand = 1 usage
	PrevStateNo Formula Condition Breakthrough
Ayu-Ayu Killer
	+ Instant Death
	+ Undefined StateDef
	+ Guard/Common State
	Pure HitDef/Projectile-Based usage also OK
Fall Instant Death
	Armor Penetration Projectile
	Indirect (5071, 5081) Fall Instant Death also OK
Various Impersonation
	Life
		= 0, with/without HPT
	Position
	Velocity
	StateNo+PrevStateNo
		Instant Death
		Hurt State
		5150
	Time
	StateType
	MoveType
		MoveType != [0,2] is used a lot in normal processing
	Alive
		= 0 (Suicide)
			Self Lose Flag Set
			Resurrection
		Negative values
		GameTime
	Anim
	PalNo
		same as alive
	GetHitVar(Damage)
	GetHitVar(Fall.Damage)
Forced Declaration of Death
	Guard State Bypass
	HitPauseTime Bypass
Helper Occupancy
Explod Occupancy
Projectile Occupancy
RNG Manipulation
and, of course, various dedicated measures
...and other things I probably forgot

=============================================
Justification
=============================================
------------------------
exec(##)
------------------------
This is merely meant to be an alternate method for ACE.
Think of it like %1024d, just condensed into a more efficient(?) form.
As long as I'm not doing something that I couldn't do with that, I think it's fine to use it.
Mostly you'll see it for lightening purposes in particular.

------------------------
Extended Variable Space (xvar)
------------------------
This is probably the most controversial addition...
The extended variable space is merely meant to be a more accessible extra region in memory.
It's basically the same thing as writing to other, unimportant parameters in a character/helper for storage purposes.
As a result, I felt like including it was fine.

------------------------
Compatability
------------------------
I tried to maximize this as much as possible.
No exploits are disabled by Beatrix, unless you modified them already in such a way that disabled them from the start (or it conflicts with Beatrix's code).
You may still use %n, %f, any text-bug related exploits, or even DBO/CMDOF exploits as you wish.
You can still use variable expansion as you wish, as well. Same with custom triggers, which I actively work around when added.
In the future, I hope to increase this even more if issues arise, but everything should work exactly as if the engine weren't tampered with to begin with.
Limiting things other people can do takes the fun away from the engine, in my opinion.

=============================================
Common issues that might pop up
=============================================
-------------------------------------------------------------------------------
Invalid trigger: xvar
-------------------------------------------------------------------------------
Beatrix crashes immediately upon startup, no error message/winmugen.exe has stopped responding
-------------------------------------------------------------------------------
You are using a character that blocks CMDOF Exploits and loaded it before Beatrix could use hers.
Either that, or a character overwrote some of the exploit hooks used by Beatrix before she could load.
Please make sure that Beatrix's CMDOF Exploit actually executed successfully, then try again.
Usually, loading her first resolves this problem.

-------------------------------------------------------------------------------
Beatrix lags... a lot
-------------------------------------------------------------------------------
She's stable at 60 FPS on my desktop, so it's quite hard to see the impact on other people's systems...
	This excludes one skill which I'm trying to look into, seemingly using sound data (Remote Shattering)
I'm actively working on resolving this issue using an older laptop as a benchmark, though.
The issue seems to be popping up after capturing too many helpers. Eventually I'd like to add some processing that deals with that.
Otherwise, I can only really recommend closing other programs to free up RAM and/or CPU processing time.

If you want to maybe help me out here, search for any code containing the word "PROFILING" and uncomment it.
This executes some normally unused code that will tell you exactly how long every player takes to execute its code, down to the microsecond.
Just let me know what's abnormally heavy.

-------------------------------------------------------------------------------
Beatrix cannot break float GameTime defenses that she's supposed to
-------------------------------------------------------------------------------
Float GameTime Formula Acquisition Requirements:
1. Anything with a higher order than 3 won't work (But there is a quasi-general purpose measure for GameTime**4)
2. The precision must be, at most, 2 digits after the decimal place.
	This is to prevent being overly precise, which will cause an inaccurate formula to be written.
	(The exceptions to this are π and e, which get automatically assumed if 3.14 or 2.72 are values)
3. The value of GameTime must be sufficiently small.
	For cubic GameTime formulae, GameTime ≤ 256
	For quadratic GameTime formulae, GameTime ≤ 4096
	For linear GameTime formulae, GameTime ≤ 16777216
If the first two conditions are satisfied, restart MUGEN or turn the GameTime reset switch on. Potentially, the reason is that the 3rd isn't...
Note that this problem shouldn't happen if the GameTime Formula Acquisition method is -3 Exploration.

-------------------------------------------------------------------------------
Can't load sora/beatrix.sff
Out of memory error.
-------------------------------------------------------------------------------
It's only 49.1 MB... I tried to optimize it to the best of my ability.
Well, at least it's not the 407.6 MB that Forbidden Knowledge has (´･ω･`)
I really don't know what to tell you about this... if you aren't already using the 4GB patch, maybe that would help?

-------------------------------------------------------------------------------
She's crashing/freezing at some point, and it seems to be consistent
-------------------------------------------------------------------------------
PLEASE yell at me.
I'd like to fix these problems ASAP and odds are I didn't notice this one specific interaction yet.

=============================================
Directory Listing
=============================================
!3e^+&!x.def:              Of course it's the def file
(README)stuff.txt:         This file lol
(りどみ)stuff.txt:           Japanese version of this file
beatrix.cfg:               Configuration to be loaded upon startup (If not present, it'll use defaults)
CHANGELOG.md:              I think it's obvious?
config.exe:                We don't use the def file anymore for this
config_bulk/
	If you don't like config.exe for whatever reason, this is a folder that contains every bulk config setting, including the default one.
	Check "_Usage Instructions (使用方法).txt" for more information
memo/
	Contains all of the documentation I created for this character that I feel like sharing immediately.
	AI.txt:                AI flowchart [deprecated]
	exec.cpp:              C++ source code for the longer exec functions
	exec_databank.txt:     ASM source code for the shorter exec functions
	general.txt:           All general-purpose measures that this character has [deprecated].
	log.txt:               A production log
	overflowTemplate.txt:  All-Encompassing (pretty much) Null Overflow Template
	overunder.txt:         The original, static values for overflow + underflow damage. Deprecated since the implementation of .DamageCalc
	varSpace.txt:          Variable and Address memo
	専用+準汎用+位置追跡.txt: Mostly Comprehensive Dedicated, Quasi-General Purpose, and Position Tracking measures list
sky/
	Contains all code-related data that this character uses.
	beatrix.air:           Animations
	beatrix.cns:           Constants
	beatrix_efi.st:        CMDOF Exploit (VERY FRAGILE DON'T SAVE IT OR YOU DIE)
	beatrix_sys.st:        -2, -1, Body System-related
	beatrix_aes.st:        Beatrix Visual Processing States
	beatrix_hlp.st:        Self Helper-related
	beatrix_enm.st:        Enemy-related
	beatrix_spc.st:        "Thematic" Dedicated Measures
	beatrix_aib.st:        AI Patterns (executed by the root)
	beatrix_aio.st:        AI Patterns (executed by the overseer helper)
	beatrix_unk.st:        Now what could this be...
	beatrix.cmd:           Commands and the constant portion of Devil's Eye Killer
	beatrix_dek.st:        StateNo Defense Breakthrough Buffer (Devil's Eye Killer)
	bootstrap.efi:         Code launched by CMDOF Exploit
sora/
    Contains all aesthetic-related data that this character uses.
	beatrix.act:           Palette Backup (the other palettes are here too, if you need them)
	beatrix.sff:           Sprites
	beatrix.snd:           Sounds

If something else is here, I probably forgot to remove it lol

=============================================
Y-Mode
=============================================
Upon enabling a certain setting, Beatrix's processing on the 2p side will drastically change.
In essence, she becomes a defeat challenge for other nukes to try to beat.
Note that in this mode, all assumptions about being a nuke are thrown out the window;
upon satisfying the penalty condition, she will immediately use Judgment Tampering to win the match.
	I've taken countermeasures against infiltration for this only.
This is merely for time-saving purposes, since you can't win if the condition is proc'd anyway.

The conditions are based around a certain Mashiro edit, but aren't necessarily identical.
General-purpose is completely infeasible; it's about as 4.9/5.0 as you can get.
	But if your general-purpose strength is good, perhaps you can adjust some of it for here...
Despite seeming like it would require it, defeat has been verified without code execution.

=============================================
Something something bio
=============================================
To save space and allow for images to be displayed, the bio for this character may be found here:
https://gitlab.com/Pikachuun/Beatrix-Repha/wikis/bio/ENG
It's not necessary to read it, but I always appreciate people reading my stuff (´･ω･`)

=============================================
Special Thanks
=============================================
The really important people
-----------------------
Oracle (http://oracle333.blog.fc2.com/)
Letter of Challenge is my inspiration for this character's design.
Without LoC, this character, or even the concept of the "Sky world" in general, wouldn't have been made.
Thanks for making such an interesting character!

lunatic (http://lunatic284.blog90.fc2.com/)
I used Mashiro-Fang as a base since I couldn't find the original Mashiro.
Additionally, the address log posted was the starting point for my current interest in MUGEN. Thank you very much.
Later I found out that shirokuma probably made the original, so I'll take this time to thank them as well.

----------
The artists
-----------------------
Reileky (https://twitter.com/Reileky)
Made the default select portrait and Forced Declaration of Death cutin (S8000,0+S8000,1).
I absolutely love how everything turned out; his artwork is truly amazing and he's a great friend.
Please keep making bongo cat animations.

八式鬼ン驍 (https://twitter.com/spattackdollmk8)
Made the monochrome select portrait.
Even with the 2-color limit they set for themselves, they put a lot of detail into their works. Truly a powerful artist.

gondwana (https://twitter.com/ususALwana)
Made the cute select portrait.
Drawing adorable things is their specialty. They also make some great characters.

DOVA-SYNDROME (https://dova-s.jp/), TSMB2 (https://www.youtube.com/user/TheSuperMarioBros2)
Created the music used in this work.
I enjoy listening to your songs a lot. Please continue producing quality tracks!

黒い羊 (https://galleria.emotionflow.com/86330/gallery.html | https://ux.getuploader.com/kyouakuchar_hinan1/)
While there isn't a contribution from this user yet in this character, I'm listing them here because of the quantity of works they've drawn.
I call them "Stained Glass God" because of the beautiful portraits they made in that style, but their other works are great too.

Tayutama (https://www.pixiv.net/users/262147), Metis (https://onedrive.live.com/?authkey=%21ALwRK0_m1ExOc8s&id=7F00D283EF8AA4FF%21128&cid=7F00D283EF8AA4FF)
Made and modified, respectively, one of the cutins (S8001,0~2).
I'll probably implement it general-purpose eventually, but for now it just appears in SouShiki_X's dedicated measure.
It's truly an honor to have one of my silly dedicated measures be corresponded to by someone I look up to as an author these days. I hope it's fine to use ('.w.')

----------
The cool people
-----------------------
MSLabo102 (https://twitter.com/mslabo102)
Helped with the Japanese version of this README and Beatrix's bio, as well as retranslated the CONFIG options.
I really appreciate you putting in the work!

skeleton (http://skeleton12.blog.fc2.com/)
I looked at some code as a reference (Persistent Tampering, etc.)
Byakko and G-Mizuka_SK were my original strength goals, as well, which I hope to surpass... eventually.

ZAF (http://zaf112.seesaa.net/)
I watched FS-Mizuchi very closely (in battle and in code) as a starting point for some protocols and the AI pattern system.
Keep doing your best, but one of these days I'll win! (lol)

sinoha (https://sinomugen.blog.fc2.com/)
A fellow MUGEN disassembler.
Forced me to make the shift from Var(7###) to XVar(###) by publishing information about the Var(Address) engine modification.

＠ひたち (http://hitachi5300.blog.fc2.com/)
I used some code as a reference.
In particular, thanks for informing people about MoveType = 3. I switched over to MoveType = 7 eventually, but I used it a lot before that.

CyberAkumaTV (https://twitter.com/CyberAkumaTv)
Gave advice about the DBO exploit used (HeapAlloc vs. VirtualAlloc, etc.).
I greatly appreciate your input. When are you going to finish Void?

Awino (http://blog.livedoor.jp/awino/)
I used some code (∅, -3 Exploration Template, Mithrandir, etc.) as a reference.
The template in particular taught me some things I didn't know. Continue to do your best!

illness (http://illness.blog.fc2.com/)
I used some code (TimeEater, etc.) as a reference.
Please continue making more strong characters in the future!

JQKJ (http://mugen744.blog.fc2.com/)
I used some code (D.P.Syaoren, etc.) as a reference.
It's very interesting to see what another ACE god character can do. I'm looking forward to the next!

en (https://ameblo.jp/aurasannnoshoukaideath/)
I used some code (P-Amber, D-Parsee, G-Sayuri, etc.) as a reference.

Nuclear Strawberry (http://entanma.com/ || https://twitter.com/AzureLazuline)
I used Copy Kitty as a reference for some self-made visuals (AI Pattern HUD).
Copy Kitty is a fun, beautiful game. I'm sure your next project will be just as enjoyable to play.

Roughly a billion people that I'm forgetting about
Created the visual effects/sound effects that I didn't create myself.
Thak you for allowing me to use your stuff in my own work!

----------
and, of course
-----------------------
You
For downloading and reading the README!
I'm honestly impressed you read it all. Was everything in here okay? Please let me know (´･ω･`)

Sincerely,
~Choon




















I guess if you want to see what other things I've made over the years:
theorem
Y-Gefühl
ασιν ινβ [ASYuuka]
(h4to89
ノヨ
and more to come.










